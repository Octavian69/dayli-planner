import { JwtHelperService } from '@auth0/angular-jwt';
import { ITokens } from '../interfaces/ITokens';
import { tokenGetter } from '../handlers/auth.handlers';
import { TTokenUser, TRefreshToken } from '../types/auth-interfaces.types';
import { Injectable } from '@angular/core';
import { ServicesModule } from 'src/app/app-page/modules/services.module';
import { StorageService } from 'src/app/app-page/services/storage.service';

@Injectable({
    providedIn: ServicesModule
})
export class JWTService {

    constructor(
        public jwtHelperService: JwtHelperService,
        public storage: StorageService
    ) {}

    decodeToken<T>(token: string): T {
        return this.jwtHelperService.decodeToken(token);
    }

    updateTokens(tokens: ITokens): void {
        this.storage.setItem<ITokens>('tokens', tokens);
    }

    getRefreshBody(): TRefreshToken {
        const { refresh_token } = this.storage.getItem<ITokens>('tokens');
        const { _id } = this.decodeToken(refresh_token);

        return { refresh_token, _id };
    }
}