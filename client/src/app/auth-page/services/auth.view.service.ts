import { Injectable } from '@angular/core';
import { Router, Params } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';
import { JWTService } from './jwt.service';
import { AuthService } from './auth.service';
import { AuthFlags } from '../flags/AuthFlags';
import { StorageService } from 'src/app/app-page/services/storage.service';
import { User } from 'src/app/app-page/models/User';
import { IMessage } from 'src/app/app-page/interfaces/app/IMessage';
import { ITokens } from '../interfaces/ITokens';
import { TLoginUser, TTokenUser } from '../types/auth-interfaces.types';
import { DBAuth } from '../db/auth.db';
import { dateToQuery } from 'src/app/system-page/dayli-planner-page/handlers/dayli-planner.handlers';


@Injectable()
export class AuthViewService extends ServiceManager<AuthFlags> {

    user$: BehaviorSubject<TTokenUser> = new BehaviorSubject(this.storage.getItem('user'));

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private jwtService: JWTService,
        private storage: StorageService,
        private authService: AuthService,
    ) {
        
        super(new AuthFlags, null, DBAuth);
    }

    getLoggedStatus(): boolean {
        const isLogged = this.storage.getItem<boolean>('isLogged');

        return isLogged;
    }

    registration(user: User): void {
        this.setFlag('isRegistrationReq', true);

        const next = (response: IMessage) => {
            const { message } = response;

            this.toastr.success(message, 'Успешно!');
            this.setFlag('isRegistrationReq', false);
            this.router.navigate(['/login']);
        }

        const error = (e) => {
            this.setFlag('isRegistrationReq', false);
            this.detected();
        }

        this.authService.registration(user)
        .pipe(takeUntil(this.unubscriber$))
        .subscribe({ next, error });
    }

    login(loginUser: TLoginUser): void {
        this.setFlag('isLoginReq', true);

        const next = (tokens: ITokens) => {
            const user: TTokenUser = this.jwtService.decodeToken(tokens.access_token);
            const queryParams: Params = { date: dateToQuery(new Date()) }
            this.storage.setItem('tokens', tokens);
            this.storage.setItem('user', user);
            this.storage.setItem('isLogged', true);


            this.user$.next(user);
            this.setFlag('isLoginReq', false);
            this.router.navigate(['/dayli-planner'], { queryParams });
        }

        const error = (e) => {
            this.setFlag('isLoginReq', false);
            this.detected();
        }

        this.authService.login(loginUser)
        .pipe(takeUntil(this.unubscriber$))
        .subscribe({ next, error });
    }

    logout(): void {
        this.router.navigate(['/login']);
        this.user$.next(null);
        this.storage.reset();
        this.destroy();
    }

    destroy(): void {
        super.reset(new AuthFlags);
    }
}