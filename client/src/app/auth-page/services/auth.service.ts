import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TRefreshToken, TLoginUser } from '../types/auth-interfaces.types';
import { ITokens } from '../interfaces/ITokens';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/app-page/interfaces/models/IUser';
import { IMessage } from 'src/app/app-page/interfaces/app/IMessage';
import { User } from 'src/app/app-page/models/User';

@Injectable()
export class AuthService {

    constructor(
        private http: HttpClient
    ) {}

    registration(user: User): Observable<IMessage> {
        return this.http.post<IMessage>('@/auth/registration', user);
    }

    login(body: TLoginUser): Observable<ITokens> {
        return this.http.post<ITokens>('@/auth/login', body);
    }

    refresh(body: TRefreshToken): Observable<ITokens> {
        return this.http.post<ITokens>('@/auth/refresh', body)
    }

    getUserByLogin(Login: string): Observable<IUser> {
        return this.http.post<IUser>('@/auth/get-by-login', { Login });
    }
}