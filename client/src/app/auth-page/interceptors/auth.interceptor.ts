import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, throwError, EMPTY } from 'rxjs';
import { catchError, switchMap, retryWhen, delay, map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { ITokens } from 'src/app/auth-page/interfaces/ITokens';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthViewService } from 'src/app/auth-page/services/auth.view.service';
import { AuthService } from 'src/app/auth-page/services/auth.service';
import { JWTService } from 'src/app/auth-page/services/jwt.service';
import { TRefreshToken } from 'src/app/auth-page/types/auth-interfaces.types';
import { prefix } from '../../app-page/handlers/app-shared.handlers';
import { DBMessages } from '../../app-page/db/app-msgs.db';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';


@Injectable()
export class AuthInterceptor extends ServiceManager<null, null> implements HttpInterceptor {
    
    private refresh$: Subject<HttpHeaders> = new Subject;
    private isRefreshed: boolean = false;
    private MSGCredential: string = this.getDataByArrayKeys(['credentials']);
    

    constructor(
        private toastr: ToastrService,
        private jwtService: JWTService,
        private authService: AuthService,
        private authViewService: AuthViewService,
    ) {
        super(null, null, DBMessages);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { API_URL } = environment;
         
        const url: string = req.url.replace('@', `${API_URL}`);
        const clonedReq: HttpRequest<any> = req.clone({url});
         

        return next.handle(clonedReq).pipe(
            catchError((err: HttpErrorResponse) => {
                switch(err.status) {
                    case 426: {
                        return this.refreshTokenHandler(req, next);
                    }

                    case 401: {
                        this.toastr.error(this.MSGCredential, 'Внимание!');
                        this.authViewService.logout();
                        break;
                    } 

                    default: {
                        this.toastr.error(err.error?.message || err.error[0]?.msg || err.error, 'Внимание!');
                    }
                 }

                 return throwError(err);
            }),
            retryWhen(errors => errors.pipe(
                delay(200),
                map(err => {
                    console.log('Retry then:', err);
 
                    if(err?.status !== 0) throw err;

                    return null;
                })
            ))
        )
    }

    private refreshTokenHandler(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url: string = req.url.replace('@', `${environment.API_URL}`);

        if(!this.isRefreshed) {
            this.isRefreshed = true;
    
            const refreshBody: TRefreshToken = this.jwtService.getRefreshBody();
    
    
            return this.authService.refresh(refreshBody).pipe(
                catchError((error: HttpErrorResponse) => {
                    this.toastr.error(this.MSGCredential, 'Внимание!');
                    this.authViewService.logout();
                    return throwError(error);
                }),
                switchMap((tokens: ITokens) => {
                    const { access_token } = tokens;
                    const authorization: string = prefix(access_token, 'Bearer ');
                    const headers: HttpHeaders = new HttpHeaders({authorization});
    
                    const newReq = req.clone({
                        url,
                        headers
                    })
    
                    this.jwtService.updateTokens(tokens);
                    this.refresh$.next(headers);
                    this.isRefreshed = false;
                    
                    return next.handle(newReq);
                })
            )
        } 
        
        return this.refresh$.pipe(
            switchMap((headers: HttpHeaders) => {
                if(headers) {
                    const newReq = req.clone({
                        url,
                        headers
                    });
    
                    return next.handle(newReq);
                }
                
                return EMPTY;
            })
        )
    }
}


// function refreshHandler(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

//     const url: string = req.url.replace('@', `${environment.API_URL}`);

//     if(!this.isRefreshed) {
//         this.isRefreshed = true;

//         const refreshBody: TRefreshToken = this.jwtService.getRefreshBody();


//         return this.authService.refresh(refreshBody).pipe(
//             catchError((error: HttpErrorResponse) => {
//                 this.toastr.error(this.MSGCredential, 'Внимание!');
//                 this.authViewService.logout();
//                 return throwError(error);
//             }),
//             switchMap((tokens: ITokens) => {
//                 const { access_token } = tokens;
//                 const authorization: string = prefix(access_token, 'Bearer ');
//                 const headers: HttpHeaders = new HttpHeaders({authorization});

//                 const newReq = req.clone({
//                     url,
//                     headers
//                 })

//                 this.jwtService.updateTokens(tokens);
//                 this.refresh$.next(headers);
//                 this.isRefreshed = false;
                
//                 return next.handle(newReq);
//             })
//         )
//     } 
    
//     return this.refresh$.pipe(
//         switchMap((headers: HttpHeaders) => {
//             if(headers) {
//                 const newReq = req.clone({
//                     url,
//                     headers
//                 });

//                 return next.handle(newReq);
//             }
            
//             return EMPTY;
//         })
//     )
// }