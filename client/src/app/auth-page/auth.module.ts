import { NgModule } from '@angular/core';
import { CoreModule } from '../app-page/modules/core.module';
import { AuthService } from './services/auth.service';
import { AuthViewService } from './services/auth.view.service';
import { AuthRoutingModule } from './auth.routing.module';
import { AuthPageComponent } from './components/auth-page/auth-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthGuard } from './guards/auth.guard';
import { MaterialModule } from '../app-page/modules/material.module';
import { ComponentsModule } from '../app-page/modules/components.module';


@NgModule({
    declarations: [
        AuthPageComponent, 
        LoginComponent, 
        RegistrationComponent
    ],
    imports: [
        CoreModule,
        MaterialModule,
        ComponentsModule,
        AuthRoutingModule
    ],
    providers: [
        AuthService,
        AuthViewService,
        AuthGuard
    ]
})
export class AuthModule {}