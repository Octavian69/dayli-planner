import { ITokens } from '../interfaces/ITokens';

export function tokenGetter(): string {
    const tokens: string = window.sessionStorage.getItem('tokens');
    
    return tokens ? (JSON.parse(tokens) as ITokens).access_token : null;
} 