import { ILength } from 'src/app/app-page/interfaces/app/ILength';

export namespace AuthLength {
    export const Login: Readonly<ILength> = {
        min: {
            Password: 7 
        },
        max: {
            Password: 20 
        },
    };

    export const Registration: Readonly<ILength> = {
        min: {
            Name: 3,
            Password: 7 
        },
        max: {
            Name: 30,
            Password: 20, 
            get EqualPassword() {
                return this.Password;
            }
        },
    };
}