import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthViewService } from '../../services/auth.view.service';
import { AuthFlags } from '../../flags/AuthFlags';
import { ANScaleState } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
  animations: [ANScaleState()]
})
export class AuthPageComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private viewService: AuthViewService
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.viewService.reset(new AuthFlags)
  }

  getHeaderText(): string {
    return this.router.isActive('/login', true) ? 'Логин' : 'Регистрация';
  }

}
