import { Component, OnInit, OnDestroy } from '@angular/core';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { UntilDestroy } from '@ngneat/until-destroy';
import { AuthViewService } from '../../services/auth.view.service';
import { AuthFlags } from '../../flags/AuthFlags';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { User } from 'src/app/app-page/models/User';
import { ILength } from 'src/app/app-page/interfaces/app/ILength';
import { AuthLength } from '../../namespaces/auth-form.namespaces';
import { AppValidator } from 'src/app/app-page/forms/models/AppValidator';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/app-page/interfaces/models/IUser';
import { TCustomValidators } from 'src/app/app-page/forms/types/app-form.types';

@UntilDestroy()
@Component({
  selector: 'dp-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, OnInitMetatdata, OnDestroy {

  //Component
  credentialForm: FormGroup;
  passwordForm: FormGroup;
  equalPassword: string = '';
  hidePassword: boolean = true;
  hideEqualPassword: boolean = true;

  //Metadata
  flags: AuthFlags;
  lengths: Readonly<ILength>;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private viewService: AuthViewService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy() {}

  ngOnInitMetatdata(): void {
    this.flags = this.viewService.getFlags();
    this.lengths = AuthLength.Registration;
  }

  init(): void {
    this.ngOnInitMetatdata();
    this.initForms();
  }

  initForms(): void {
    this.credentialForm = this.fb.group({
      Login: [null, 
        [
          Validators.required,
          Validators.email
        ], 
        [
          AppValidator.withoutSpace(),
          AppValidator.requestDebounce(this.isExistEmail())
        ]
      ],
      Name: [null,
        [
          Validators.required,
          Validators.minLength(this.lengths.min.Name),
          Validators.maxLength(this.lengths.max.Name)
        ],
      ]    
    })

    this.passwordForm = this.fb.group({
      Password: [null, 
        [
          Validators.required,
          Validators.minLength(this.lengths.min.Password),
          Validators.maxLength(this.lengths.max.Password)
        ], 
        [
          AppValidator.withoutSpace()
        ]
      ],
      EqualPassword: [null]
    }, {
      asyncValidators: [AppValidator.debounce(this.isEqualPasswords, 0)]
    })
  }

  isExistEmail = (): TCustomValidators<string, IUser> => {
    const { authService, authService: { getUserByLogin } } = this;
    const req: (Login: string) => Observable<IUser> = getUserByLogin.bind(authService);
    const handler: (u: IUser) => ValidationErrors = (user: IUser): ValidationErrors => {
      return user 
      ? { existEmail: true }
      : null
    }

    return { req, handler };
  }

  isEqualPasswords = (formValue): ValidationErrors => {
    const { Password, EqualPassword } = formValue;
    const isEqual = Object.is(Password, EqualPassword);
    
    return !isEqual ? { notEqualPasswords: true } : null
  }
  
  showEqualMessage(): boolean {
    const isShow: boolean = 
        this.passwordForm.touched 
    &&  this.passwordForm.hasError("notEqualPasswords");

    return isShow;
  }

  getCredentialErrorStatus(): boolean {
    const { invalid, touched } = this.credentialForm;
    const { pending } = this.credentialForm.get('Login');

    return invalid || pending || !touched;
  }

  getPasswordErrorStatus(): boolean {
    const { invalid, touched, pending } = this.passwordForm;
    const isInvalid: boolean = invalid || !touched || pending;

    return isInvalid;
  }

  getSubmitStatus(): boolean {
    const isDisabled: boolean = 
          this.viewService.getFlag('isRegistrationReq')
      ||  this.credentialForm.invalid
      ||  this.passwordForm.invalid;

    return isDisabled;
  }

  registration() {
    const { value: { Login, Name } } = this.credentialForm;
    const { value: { Password } } = this.passwordForm;
    const value: Partial<User> = { Login, Name, Password };
    const user: User = Object.assign(new User, value);

    this.viewService.registration(user);
  }

}
