import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AuthViewService } from '../../services/auth.view.service';
import { AuthFlags } from '../../flags/AuthFlags';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { TLoginUser } from '../../types/auth-interfaces.types';
import { AuthLength } from '../../namespaces/auth-form.namespaces';
import { ILength } from 'src/app/app-page/interfaces/app/ILength';
import { OnDetectChanges } from 'src/app/app-page/interfaces/hooks/OnDetectChanges';
import { Observable } from 'rxjs';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';


@UntilDestroy()
@Component({
  selector: 'dp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy, OnInitMetatdata, OnDetectChanges {
  //Component
  hide: boolean = true;
  form: FormGroup;

  //Metadata
  flags: AuthFlags;
  formTitles: ISimple<string>;
  lengths: Readonly<ILength>;

  get cdr(): ChangeDetectorRef {
    return this._cdr
  }

  constructor(
    private fb: FormBuilder,
    private _cdr: ChangeDetectorRef,
    public viewService: AuthViewService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnInitMetatdata(): void {
    this.flags = this.viewService.getFlags();
    this.lengths = AuthLength.Login;
    this.formTitles = this.viewService.getDataByArrayKeys(['formTitles']);
  }

  ngOnDetectChanges(): void {
    this.viewService.getStream('detector$')
    .pipe(untilDestroyed(this))
    .subscribe(_ => {
      console.log('detect')
      this.cdr.detectChanges()
    });
  } 

  ngOnDestroy() {
    this.viewService.destroy();
  }
  
  init(): void {
    this.ngOnInitMetatdata();
    this.ngOnDetectChanges();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      Login: [null, [
        Validators.required, 
        Validators.email], []],
      Password: [null, [
        Validators.required,
        Validators.minLength(this.lengths.min.Password),
        Validators.maxLength(this.lengths.max.Password),
      ], []]
    });
  }

  getPasswordLength(): string {
    if(this.form) {
      const { value: { Password } } = this.form
      return `${Password?.length || 0} / ${this.lengths.max.Password}`
    }
  }

  login(): void {
    const { value } = this.form;

    this.viewService.login(value as TLoginUser);
  }
}
