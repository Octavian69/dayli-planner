import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate, CanActivateChild, Params } from '@angular/router';
import { AuthViewService } from '../services/auth.view.service';
import { Observable } from 'rxjs';
import { dateToQuery } from 'src/app/system-page/dayli-planner-page/handlers/dayli-planner.handlers';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(
        private router: Router,
        private authViewService: AuthViewService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    {
        const isLogged: boolean = this.authViewService.getLoggedStatus();

        if(isLogged) {
            const queryParams: Params = { date: dateToQuery(new Date()) }
            this.router.navigate(['/dayli-planner'], { queryParams });
            
            return false;
        }

        return true
        
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    {
        return this.canActivate(route, state)
    }
}