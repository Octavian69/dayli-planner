import { IUser } from 'src/app/app-page/interfaces/models/IUser';

export type TTokenUser = Exclude<IUser, 'Login' | 'Password'>;
export type TLoginUser = Pick<IUser, 'Login' | 'Password'>;
export type TRefreshToken = {
    refresh_token: string,
    _id: string
}
