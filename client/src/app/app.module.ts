import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import ruLocale from '@angular/common/locales/ru';
import { JwtModule } from "@auth0/angular-jwt";
import { ToastrModule } from 'ngx-toastr';
import { tokenGetter } from './auth-page/handlers/auth.handlers';
import { AuthInterceptor } from './auth-page/interceptors/auth.interceptor';
import { AuthModule } from './auth-page/auth.module';
import { AppRoutingModule } from './app.routing.module';
import { ServicesModule } from './app-page/modules/services.module';
import { SharedModule } from './app-page/modules/shared.module';
import { SystemModule } from './system-page/system.module';

import { AppComponent } from './app.component';

import { AppToolbarComponent } from './app-page/components/app-toolbar/app-toolbar.component';


registerLocaleData(ruLocale, 'ru');


@NgModule({
  declarations: [
    AppComponent,
    AppToolbarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        blacklistedRoutes: [ new RegExp(/api[/]auth/, 'i') ]
      }
    }),
    ToastrModule.forRoot(),
    ServicesModule.forRoot(),
    SharedModule,
    AuthModule,
    SystemModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
