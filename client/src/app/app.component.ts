import { Component } from '@angular/core';
import { AuthViewService } from './auth-page/services/auth.view.service';

@Component({
  selector: 'dp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    public authViewService: AuthViewService
  ) {}
}
