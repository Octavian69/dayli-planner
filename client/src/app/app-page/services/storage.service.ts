import { Injectable } from '@angular/core';
import { ServicesModule } from '../modules/services.module';
import { TStorage } from '../types/app-services.types';

@Injectable({
    providedIn: ServicesModule
})
export class StorageService {
    constructor() {};

    getItem<T>(key: string, storageName: TStorage = 'sessionStorage'): T {
        const value: string = window[storageName].getItem(key);

        return JSON.parse(value);
    }

    setItem<T>(key: string, value: T, storageName: TStorage = 'sessionStorage'): void {
        const strValue: string = JSON.stringify(value);

        window[storageName].setItem(key, strValue);
    }

    removeItem(key: string, storageName: TStorage = 'sessionStorage'): void {
        window[storageName].removeItem(key);
    }

    reset(): void {
        window['localStorage'].clear();
        window['sessionStorage'].clear();
    }
}