import { IPage } from '../interfaces/app/IPage';

export class Page implements IPage {
    constructor(
        public limit: number,
        public skip: number = 0,
    ){}
}