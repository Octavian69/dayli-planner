import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { DBFormErrorMsgs } from '../forms/db/form-error-msgs';
import { TSMParameter } from '../types/app-managers.types';
import { getDataByArrayKeys } from '../handlers/app-shared.handlers';

export class ServiceManager<F extends object = null, S extends object = null> {

    unubscriber$: Subject<true> = new Subject();
    detector$: Subject<true> = new Subject;

    constructor(
        protected flags: F = null,
        protected state: S = null,
        protected database: object = null
    ) {}

    getDataByArrayKeys<T>(keys: string[]): T {
        return getDataByArrayKeys(keys, this.database) as T;
    }

    getFlags(): F {
        return this.flags;
    }

    getFlag<K extends keyof F>(key: K): F[K] {
        return this.flags[key];
    }

    setFlag<K extends keyof F>(key: K, value: F[K]): void {
        this.flags[key] = value;
    }

    getState(): S {
        return this.state;
    }

    getStateValue<K extends keyof S>(key: K): S[K] {
        return this.state[key];
    }

    getMessage(path: string[] | string, db = DBFormErrorMsgs) {
        if(path instanceof Array) return getDataByArrayKeys(path, db);

        return db[path];
    }

    setStateValue<K extends keyof S>(key: K, value: S[K]): void {
        this.state[key] = value;
    }

    getStream<T>(streamKey: string): Observable<T> {
        return (this[streamKey] as Subject<T>).asObservable();
    }

    getStreamValue<T>(streamKey: string): T {
        return (this[streamKey] as BehaviorSubject<T>).getValue();
    }

    emitStream<T>(streamName: string, value: T): void {
        (this[streamName] as Subject<T>).next(value);
    }

    detected() {
        this.detector$.next(true);
    }

    reset(flags: F = null, state: S = null): void {
        if(flags) {
            Object.assign(this.flags, flags);
        }

        if(state) {
            Object.assign(this.state, state);
        }

        this.unsubscribe();
    }
    

    updateToDefaultParam(key: TSMParameter, value: F & S): void {
        this[key] = value;
    }

    unsubscribe(): void {
        this.unubscriber$.next(true);
    }
}
