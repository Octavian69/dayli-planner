//StorageService
export type TStorage = 'localStorage' | 'sessionStorage';