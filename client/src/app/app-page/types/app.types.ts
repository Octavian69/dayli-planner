export type TAppModeType = 'auth' | 'system';
export type TBtnType = 'submit' | 'button'; 
export type TSide = 'prev' | 'next';
export type TModeType = 'create' | 'edit' | 'remove';
export type TModalShow = 'show' | 'reset';
export type TShowStatus = 'open' | 'close'; 
export type TBoxPostion = 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right';

export type TKeyValue<T> = {
    key: string;
    value: T
}

export type TFromTo<T = number> = {
    from: T;
    to?: T;
}