export type TPaginationRes<T> = {
    rows: T[];
    totalCount: number;
}

export type TMessageRes = { message: string };
export type TListAction = 'replace' | 'expand';