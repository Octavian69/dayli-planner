import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { ISimple } from '../../interfaces/app/ISimple';
import { TBoxPostion } from '../../types/app.types';

@Component({
  selector: 'dp-border-side',
  templateUrl: './border-side.component.html',
  styleUrls: ['./border-side.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BorderSideComponent implements OnInit {

  @Input() offset: string = '1rem';
  @Input() height: string = '13px';
  @Input() width: string = '25px'
  @Input() borderWidth: string = '3px';
  @Input() borderColor: string = '#3f51b5';

  constructor() { }

  ngOnInit(): void {
  }

  getBorderItemStyles(position: TBoxPostion): ISimple<string> {
    const [pos1, pos2] = position.split('-');
    const { height, width } = this;

    const prop1: string = `border-${pos1}`;
    const prop2: string = `border-${pos2}`;
    const value: string = `${this.borderWidth} solid ${this.borderColor}`;

    return {
      [prop1]: value,
      [prop2]: value,
      height,
      width
    }
  }
}
