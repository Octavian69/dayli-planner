import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { TTokenUser } from 'src/app/auth-page/types/auth-interfaces.types';
import { getDataByArrayKeys } from 'src/app/app-page/handlers/app-shared.handlers';
import { TAppModeType } from 'src/app/app-page/types/app.types';
import { ToolbarDatabase } from './db/toolbar.db';
import { ILinkOption } from 'src/app/app-page/interfaces/app/ILinkOption';
import { Router, Params } from '@angular/router';
import { queryToDate, dateToQuery } from 'src/app/system-page/dayli-planner-page/handlers/dayli-planner.handlers';

@Component({
  selector: 'dp-app-toolbar',
  templateUrl: './app-toolbar.component.html',
  styleUrls: ['./app-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppToolbarComponent {

  modeType: TAppModeType = 'auth';
  options: ILinkOption[] = [];

  constructor(
    private router: Router
  ) {}

  @Input('user') set _user(value: TTokenUser) {
    this.modeType = value ? 'system' : 'auth';
    const optionsPath: string[] = ['options', this.modeType];

    this.options = getDataByArrayKeys(optionsPath, ToolbarDatabase);
  }

  @Output('logout') _logout: EventEmitter<void> = new EventEmitter();  

  naviagate(): void {
    const path = this.modeType === 'auth' ? '/login' : '/dayli-planner';
    const queryParams: Params = this.modeType === 'auth' ? {} : { date: dateToQuery(new Date) };
    
    this.router.navigate([path], { queryParams });
  }

  navigateTo(path: string): void {

    switch(path) {
      case 'logout': {
        this._logout.emit();
        break;
      }

      default: {
        this.router.navigate([path]);
      }
    }
  }

}
