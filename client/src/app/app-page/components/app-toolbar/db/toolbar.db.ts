export const ToolbarDatabase = {
    options: {
        auth: [
            {
                title: 'Вход',
                icon: 'face',
                link: '/login',
                id: 1
            },
            {
                title: 'Регистрация',
                icon: 'how_to_reg',
                link: '/registration',
                id: 2
            },
        ],
        system: [
             {
                title: 'На главную',
                icon: 'house',
                link: '/dayli-planner',
                id: 1
            },
            {
                title: 'Выйти',
                icon: 'exit_to_app',
                link: 'logout',
                id: 2
            },
        ]
    }
} as const;