import { Component, Output, EventEmitter, Input, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { ANFade, ANScaleState } from '../../animations/animations';

@Component({
  selector: 'dp-shadow',
  templateUrl: './shadow.component.html',
  styleUrls: ['./shadow.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANFade(), ANScaleState()]
})
export class ShadowComponent {

  animateState: boolean = false;
  @HostBinding('@ANFade') fade;
  @Input() contentCenter: boolean = true;
  @Output('hide') _hide = new EventEmitter<void>();

  hide(e: MouseEvent): void {
    const { target, currentTarget } = e;
    const isShadowRoot: boolean = Object.is(target, currentTarget);

    if(isShadowRoot) {
      this._hide.emit();
    }
  }

  start(e: AnimationEvent): void {
    this.animateState = !this.animateState;
  }
}
