import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dp-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  @Input() text: string;
  @Input() loading: boolean = false;
  @Output('action') _action = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  getText(): string {
    return `Вы действительно хотите ${this.text} ?`
  }

  action(flag: boolean): void {
    this._action.emit(flag);
  }

}
