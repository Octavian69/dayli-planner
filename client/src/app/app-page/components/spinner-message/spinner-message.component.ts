import { Component, Input } from '@angular/core';

@Component({
  selector: 'dp-spinner-message',
  templateUrl: './spinner-message.component.html',
  styleUrls: ['./spinner-message.component.scss']
})
export class SpinnerMessageComponent {

  @Input() diameter: number = 25;
}
