import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'dp-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BadgeComponent {
  @Input() tooltip: string;
  @Input() text: string | number;
}
