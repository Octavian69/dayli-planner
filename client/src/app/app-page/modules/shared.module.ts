import { NgModule } from '@angular/core';
import { ComponentsModule } from './components.module';
import { CoreModule } from './core.module';
import { MaterialModule } from './material.module';
import { DirectivesModule } from './directives.module';
import { PipesModule } from './pipes.module';

const modules = [
    CoreModule,
    MaterialModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule
];

@NgModule({
    imports: modules,
    exports: modules
})
export class SharedModule {}