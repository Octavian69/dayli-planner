//modules
import { NgModule } from '@angular/core';
import { CoreModule } from './core.module';
import { MaterialModule } from './material.module';
import { DirectivesModule } from './directives.module';
import { PipesModule } from './pipes.module';

//components
import { FormSubmitBtnComponent } from '../forms/components/form-submit-btn/form-submit-btn.component';
import { FormTextareaComponent } from '../forms/components/form-textarea/form-textarea.component';
import { FormInputComponent } from '../forms/components/form-input/form-input.component';
import { FormSelectComponent } from '../forms/components/form-select/form-select.component';
import { FormTimepickerComponent } from '../forms/components/form-timepicker/form-timepicker.component';
import { SpinnerMessageComponent } from '../components/spinner-message/spinner-message.component';
import { ShadowComponent } from '../components/shadow/shadow.component';
import { BorderSideComponent } from '../components/border-side/border-side.component';
import { BadgeComponent } from '../components/badge/badge.component';
import { ConfirmComponent } from '../components/confirm/confirm.component';

const components = [
    FormInputComponent,
    FormSubmitBtnComponent,
    FormTextareaComponent,
    FormSelectComponent,
    FormTimepickerComponent,
    SpinnerMessageComponent,
    ShadowComponent,
    BorderSideComponent,
    BadgeComponent,
    ConfirmComponent
];

@NgModule({
    declarations: components,
    imports: [
        CoreModule,
        MaterialModule,
        DirectivesModule,
        PipesModule
    ],
    exports: components
})
export class ComponentsModule {}