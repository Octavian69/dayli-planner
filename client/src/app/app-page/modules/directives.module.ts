import { NgModule } from '@angular/core';
import { ChangeDetectorDirective } from '../directives/change-detector.directive';
import { PageBreakDirective } from '../directives/page-break.directive';

const directives = [
    ChangeDetectorDirective,
    PageBreakDirective
];

@NgModule({
    declarations: directives,
    exports: directives
})
export class DirectivesModule {}