import { NgModule } from '@angular/core';

const pipes = [];

@NgModule({
    declarations: pipes,
    exports: pipes
})
export class PipesModule {}