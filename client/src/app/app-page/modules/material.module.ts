import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatRippleModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
// import {TextFieldModule} from '@angular/cdk/text-field';

const modules = [
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatStepperModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatRippleModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule
    // TextFieldModule
]

@NgModule({
    imports: modules,
    exports: modules
})
export class MaterialModule {}