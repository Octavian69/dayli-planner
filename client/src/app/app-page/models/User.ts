import { IUser } from '../interfaces/models/IUser';

export class User implements IUser {
    Name: string;
    Login: string;
    Password: string;
    Created: Date = new Date();

    constructor() {}
}