import { forwardRef, InjectionToken } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms'

export class FormProvider {

    provide: InjectionToken<ControlValueAccessor> = NG_VALUE_ACCESSOR;
    multi: boolean = true;
    useExisting: Function;

    constructor(component: Function) {
        this.useExisting = forwardRef(() => component);
    }
}