import { ErrorStateMatcher } from '@angular/material/core';
import { NgControl } from '@angular/forms';

export class ErrorMatcher implements ErrorStateMatcher  {

    constructor(public control: NgControl) {}

    isErrorState(): boolean {
        const { touched, invalid, pending } = this.control;
        const isError: boolean = touched  && invalid && !pending;

        return isError;
    }
}