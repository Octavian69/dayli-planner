import { Observable, BehaviorSubject } from 'rxjs';
import { ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { debounceTime, map, first, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TCustomValidators } from '../types/app-form.types';
import { copyDate } from '../../handlers/app-shared.handlers';

export class AppValidator {

    static requestDebounce<T, R>(settings: TCustomValidators<T, R> , ms: number= 400): ValidatorFn {
        const { req, handler } = settings;
        const trigger$: BehaviorSubject<ValidationErrors> = new BehaviorSubject(null);
        const error$: Observable<ValidationErrors> = trigger$.pipe(
            distinctUntilChanged(),
            debounceTime(ms),
            switchMap(req),
            map(handler),
            first()
        )

        return (control: AbstractControl): Observable<ValidationErrors> => {
            trigger$.next(control.value as T);

            return error$;
        }
    }

    static debounce<T>(handler:(value: T) => ValidationErrors, ms: number = 400): ValidatorFn {
        const trigger$: BehaviorSubject<T> = new BehaviorSubject(null);
        const error$: Observable<ValidationErrors> = trigger$.pipe(
            distinctUntilChanged(),
            debounceTime(ms),
            map(handler),
            first()
        )

        return (control: AbstractControl): Observable<ValidationErrors> => {
            trigger$.next(control.value);

            return error$;
        }
    }

    static withoutSpace(ms: number = 300): ValidatorFn { 
        const handler = (str: string) => {
            return str?.includes(' ')
                ? { space: true }
                : null
        }

        return this.debounce(handler, ms);
    }

    static minTime(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors => {
            const { value } = control;
            const controlDate: number = copyDate(value).getTime();
            const now: number = new Date().getTime();
            const isLess: boolean = controlDate < now;

            return isLess ? { minTime: true } : null;
        }
    }

    static maxTime(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors => {
            const { value } = control;
            const controlDate: number = copyDate(value).getTime();
            const now: number = new Date().getTime();
            const isMore: boolean = controlDate > now;

            return isMore ? { maxTime: true } : null;
        }
    }
}