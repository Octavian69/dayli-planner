import { Component, OnInit, ChangeDetectorRef, Injector, Input, ChangeDetectionStrategy } from '@angular/core';
import { completeErrorMessage } from '../../handlers/form.handlers';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { NgControl, ControlValueAccessor } from '@angular/forms';
import { ErrorMatcher } from '../../models/ErrorStateMatcher';
import { FormProvider } from '../../models/FormProvider';
import { IFormTextarea } from '../../interfaces/IFormTextArea';

@Component({
  selector: 'dp-form-textarea',
  templateUrl: './form-textarea.component.html',
  styleUrls: ['./form-textarea.component.scss'],
  providers: [new FormProvider(FormTextareaComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormTextareaComponent implements OnInit, ControlValueAccessor, IFormTextarea {

  ngControl: NgControl;
  errorMatcher: ErrorMatcher;
  
  value: any = null;
  disabled: boolean = false;

  onChange = (v) => {};
  onTouch = () => {};

  //shared
  @Input() label: string;
  @Input() placeholder: string;
  @Input() customHint: boolean = false;


  //control
  @Input() maxlength: number = null;
  @Input() minlength: number = null;
  @Input() trim: boolean = true;
  @Input() lengthHint: boolean = false;
  @Input() minRows: number = 3;

  constructor(
    private injector: Injector,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.initControl();
  }

  initControl(): void {
    this.ngControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.ngControl);
    this.cdr.detectChanges();
  }


  setValue(value: string): void {
    this.value = this.trim ? value.trim().replace(/[ ]+/, ' ') : value;
    this.onChange(this.value);
    this.onTouch();
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouch = fn;
  }

  writeValue(value: any): void {
    this.value = value;
    this.cdr.detectChanges();
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
    this.cdr.detectChanges();
  }

  getErrorMessage(error: TKeyValue<any>): string {
    return completeErrorMessage(error, this.label);
  }
}
