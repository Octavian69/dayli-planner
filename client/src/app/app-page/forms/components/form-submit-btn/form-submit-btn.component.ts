import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { TBtnType } from '../../../types/app.types';
import { ISimple } from '../../../interfaces/app/ISimple';

@Component({
  selector: 'dp-form-submit-btn',
  templateUrl: './form-submit-btn.component.html',
  styleUrls: ['./form-submit-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormSubmitBtnComponent {

  isHover: boolean = false;

  @Input() text: string = 'Отправить'
  @Input() type: TBtnType = 'submit'
  @Input() bgColor: string = '#3f51b5';
  @Input() hoverBgColor: string = 'linear-gradient(to right, rgb(192, 36, 37), rgb(240, 203, 53))';
  @Input() fontColor: string = '#fff';
  @Input() disabled: boolean = false;
  @Input() showLoader: boolean = false;
  @Output('action') _action: EventEmitter<void> = new EventEmitter();

  getBtnBgColor(): ISimple<string> {
    return {
      background: this.isHover ? this.hoverBgColor : this.bgColor,
      color: this.fontColor
    }
  }

  hoverHandler(flag: boolean): void {
    this.isHover = flag;
  }
  
  action(): void {
    this._action.emit();
  }
}
