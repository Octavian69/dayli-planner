import { Component, OnInit, Input, Injector, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormProvider } from '../../models/FormProvider';
import { IFormSelect } from '../../interfaces/IFormSelect';
import { NgControl, ControlValueAccessor } from '@angular/forms';
import { ErrorMatcher } from '../../models/ErrorStateMatcher';
import { completeErrorMessage } from '../../handlers/form.handlers';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';

@Component({
  selector: 'dp-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss'],
  providers: [new FormProvider(FormSelectComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormSelectComponent implements OnInit, ControlValueAccessor, IFormSelect {

  value: any;
  disabled: boolean = false;
  ngControl: NgControl;
  errorMatcher: ErrorMatcher

  onChange: (v: any) => void = (v: any) => {};
  onTouch: () => void = () => {};

  //shared
  @Input() placeholder: string;


  //control
  @Input() label: string;
  @Input() options: any[] = [];
  @Input() bindTitle: string = 'title';
  @Input() bindValue: string = 'id';
  @Input() customHint: boolean = false;
  @Input() getOptionsStyle: (opt: any) => ISimple = null
  @Input() disabledOptFn: (opt: any) => boolean = null;

  constructor(
    private injector: Injector,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.initControl();
  }

  initControl(): void {
    this.ngControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.ngControl);
    this.cdr.detectChanges();
  }

  setValue(value: any): void {
    this.value = value;
    this.onChange(value);
    this.onTouch();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
  }

  getErrorMessage(error: TKeyValue<any>): string {
    return completeErrorMessage(error, this.label);
  }
}
