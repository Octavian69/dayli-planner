import { Component, OnInit, Input, ChangeDetectionStrategy, Injector } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { IFormTimePicker } from '../../interfaces/IFormTimepicker';
import { FormProvider } from '../../models/FormProvider';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { completeErrorMessage } from '../../handlers/form.handlers';
import { TTimeRange, TTimePicker, TMathAction, TTimeGetter, TTimeSetter } from '../../types/app-form.types';
import { NSForm } from '../../namespaces/forms.namespaces';
import { ErrorMatcher } from '../../models/ErrorStateMatcher';
import { createMap, getDataByArrayKeys, copyDate } from 'src/app/app-page/handlers/app-shared.handlers';
import { DBForms } from '../../db/form.db';

@Component({
  selector: 'dp-form-timepicker',
  templateUrl: './form-timepicker.component.html',
  styleUrls: ['./form-timepicker.component.scss'],
  providers: [new FormProvider(FormTimepickerComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormTimepickerComponent implements OnInit, ControlValueAccessor, IFormTimePicker {

  limits: TTimeRange = NSForm.TIME_RANGE; 

  value: Date = new Date;
  currentDate: Date = new Date;
  disabled: boolean = false;
  ngControl: NgControl;
  errorMatcher: ErrorMatcher;
  methods: Map<TTimePicker, [TTimeGetter, TTimeSetter]> = createMap(getDataByArrayKeys(['timeMethods'], DBForms))

  onChange: (v: any) => void;
  onTouch: () => void;

  //shared
  @Input() label: string = 'Время';
  @Input() customHint: boolean = false;

  //control

  @Input() minDate: Date;
  @Input() maxDate: Date;

  constructor(
    private injector: Injector
  ) { }
  

  ngOnInit(): void {
    this.initControl();
  }

  initControl(): void {
    this.ngControl = this.injector.get(NgControl, null);

    if(this.ngControl) {
      this.errorMatcher = new ErrorMatcher(this.ngControl);
    }
  }

  getValue(type: TTimePicker): string {
    const [ hours, minutes ] = this.value.toLocaleTimeString().split(':');

    return type === 'hours' ? hours : minutes;
  }

  getTooltip(action: TMathAction): string {
    return action === 'dec' ? 'Уменьшить' : 'Увеличить';
  }

  step(type: TTimePicker, action: TMathAction): void {
    const [getter, setter] = this.methods.get(type);
    const step: number = action === 'inc' ? 1 : -1;
    const value: number = this.value[getter]() + step;
    
    this.value[setter](value);
    
    this.currentDate[setter](this.value[getter]());

    this.onChange(this.currentDate.getTime());
    this.onTouch();
  }

  wheel(e: MouseWheelEvent, type: TTimePicker): void {
    if(!this.disabled) {
      const { deltaY } = e;
      const action: TMathAction = deltaY < 0 ? 'inc' : 'dec';
  
      this.step(type, action);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(value: Date): void {
    const date: Date = copyDate(value) || new Date;

    this.value = date;
    this.currentDate = copyDate(date);
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
  }

  getErrorMessage(error: TKeyValue<any>): string {
    return completeErrorMessage(error, this.label);
  }

}
