import { Component, OnInit, Input, Injector, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { ControlValueAccessor, NgControl} from '@angular/forms';
import { IFormInput } from '../../interfaces/IFormInput';
import { FormProvider } from '../../models/FormProvider';
import { TKeyValue } from '../../../types/app.types';
import { ErrorMatcher } from '../../models/ErrorStateMatcher';
import { completeErrorMessage } from '../../handlers/form.handlers';

@Component({
  selector: 'dp-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers: [new FormProvider(FormInputComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormInputComponent implements OnInit, ControlValueAccessor, IFormInput {
  
  ngControl: NgControl;
  errorMatcher: ErrorMatcher;
  
  value: any = null;
  disabled: boolean = false;
  hide: boolean = true;

  onChange = (v) => {};
  onTouch = () => {};

  //shared
  @Input() label: string;
  @Input() placeholder: string;
  @Input() customHint: boolean = false;


  //control
  @Input() type: 'password' | 'text' = 'text';
  @Input() maxlength: number = null;
  @Input() minlength: number = null;
  @Input() trim: boolean = true;
  @Input() lengthHint: boolean = false;
  @Input() hideHint: boolean = false;

  constructor(
    private injector: Injector,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.initControl();
  }

  initControl(): void {
    this.ngControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.ngControl);
    this.cdr.detectChanges();
  }

  setHideState(): void {
    this.hide= !this.hide;
    this.type = this.type === 'password' ? 'text' : 'password';
  }

  setValue(value: string): void {
    this.value = this.trim ? value.trim().replace(/[ ]+/, ' ') : value;
    this.onChange(this.value);
    this.onTouch();
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouch = fn;
  }

  writeValue(value: any): void {
    this.value = value;
    this.cdr.detectChanges();
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
    this.cdr.detectChanges();
  }

  getErrorMessage(error: TKeyValue<any>): string {
      return completeErrorMessage(error, this.label);
  }

}
