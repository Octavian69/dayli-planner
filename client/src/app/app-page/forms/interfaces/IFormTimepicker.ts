import { TTimeRange, TTimePicker, TMathAction, TTimeGetter, TTimeSetter } from '../types/app-form.types';
import { NgControl } from '@angular/forms';
import { ErrorMatcher } from '../models/ErrorStateMatcher';
import { TKeyValue } from '../../types/app.types';

export interface IFormTimePicker {
    limits: TTimeRange; 
    value: Date;
    currentDate: Date;
    methods: Map<TTimePicker, [TTimeGetter, TTimeSetter]>;
    label: string
    disabled: boolean;
    ngControl: NgControl;
    errorMatcher: ErrorMatcher;
  
    onChange: (v: any) => void;
    onTouch: () => void;
    initControl(): void;
    step(type: TTimePicker, action: TMathAction): void;
    wheel(e: MouseWheelEvent, type: TTimePicker): void;
    getErrorMessage(error: TKeyValue<any>): string
  
}