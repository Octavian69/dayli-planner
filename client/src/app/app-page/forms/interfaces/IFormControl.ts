import { NgControl } from '@angular/forms';
import { TKeyValue } from '../../types/app.types';
import { ErrorMatcher } from '../models/ErrorStateMatcher';

export class IFormControl {
    label: string;
    placeholder: string;
    disabled: boolean;
    ngControl: NgControl;
    customHint: boolean;
    errorMatcher?: ErrorMatcher;
    setValue: (v: any, ...args) => void;
    onChange: (v: any) => void;
    onTouch: () => void;
    initControl: () => void
    getErrorMessage: (error: TKeyValue<any>) => string;
}