import { IFormControl } from './IFormControl';

export interface IFormTextarea extends IFormControl {
    maxlength: number;
    minlength: number;
    minRows: number;
    lengthHint: boolean;
    trim: boolean;
}