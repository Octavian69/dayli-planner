import { IFormControl } from './IFormControl';
import { ISimple } from '../../interfaces/app/ISimple';

export interface IFormSelect extends IFormControl {
    options: any[];
    bindValue: any;
    bindTitle: string;
    disabledOptFn: (opt: any) => boolean;
    getOptionsStyle: (opt: any) => ISimple
}