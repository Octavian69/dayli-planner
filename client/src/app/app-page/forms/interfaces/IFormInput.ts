import { IFormControl } from './IFormControl';

export class IFormInput extends IFormControl {
    type: 'text' | 'password' | 'number';
    maxlength: number;
    minlength: number;
    lengthHint: boolean;
    trim: boolean;
    hide: boolean;
    setHideState: () => void;
}