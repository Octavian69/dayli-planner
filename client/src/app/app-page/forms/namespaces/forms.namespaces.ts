import { TTimeRange } from '../types/app-form.types';

export namespace NSForm {

    export const TIME_RANGE: TTimeRange = {
        min: {
            hours: 0,
            minutes: 0
        },
        max: {
            hours: 23,
            minutes: 59
        }
    }
}