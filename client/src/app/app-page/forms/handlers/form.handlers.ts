
import { TKeyValue } from '../../types/app.types';
import { getDataByArrayKeys } from '../../handlers/app-shared.handlers';
import { DBFormErrorMsgs } from '../db/form-error-msgs';
import { TFormErrorMetadata } from '../types/app-form.types';


export function completeErrorMessage(error: TKeyValue<any>, fieldName: string): string {
    const { key: errorKey, value } = error;
    const message: string = getDataByArrayKeys([errorKey], DBFormErrorMsgs);
    let errorValue: TFormErrorMetadata;

    if(typeof value === 'object') {
        errorValue = Object.assign({}, value, { fieldName });
    } else {
        errorValue = { fieldName };
    }

    const msg: string = Object.entries(errorValue).reduce((accum: string, [key, value]) => {
        key = `{{${key}}}`; // все заменяемые ключи в строке ошибки в DBFormErrorMsgs должны быть выделены двойными фигурными скобками
        
        if(accum.includes(key)) {
            accum = accum.replace(key, value);
        }

        return accum;
        
    }, message);

    return msg;
}