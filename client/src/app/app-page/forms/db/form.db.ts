export const DBForms = {
    timeMethods: [
        ['hours', ['getHours', 'setHours']],
        ['minutes', ['getMinutes', 'setMinutes']],
    ]
};
