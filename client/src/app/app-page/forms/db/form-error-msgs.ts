export const DBFormErrorMsgs = {
    required: 'Поле "{{fieldName}}" обязательно для заполнения.',
    minlength: 'Минимальная длина поля "{{fieldName}}": {{requiredLength}} символов.Cейчас {{actualLength}}',
    maxlength: 'Максимальная длина поля "{{fieldName}}": {{requiredLength}} символов.Cейчас {{actualLength}}',
    email: 'Поле "{{fieldName}}" должно быть типа email: ivanv@gmail.com',
    space: 'Поле "{{fieldName}}" не должно иметь пробелов.',
    existEmail: 'Пользователь с таким email уже зарегистрирован.',
    notEqualPasswords: 'Пароли не совпадают',
    minTime: 'Время не может быть меньше текущего',
    maxTime: 'Время не может быть больше текущего'
} as const;