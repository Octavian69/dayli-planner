import { Observable } from 'rxjs';
import { ValidationErrors } from '@angular/forms';
import { ILength } from '../../interfaces/app/ILength';

export type TControlStatus = 'enable' | 'disable';
export type TMathAction = 'inc' | 'dec';
export type TFormError = 'default' | 'default-object' | 'custom';
export type TFormModeType = 'create' | 'edit';
export type TTimePicker = 'minutes' | 'hours';
export type TTimeRange = Record<keyof ILength, { 'hours': number, 'minutes': number }>;

export type TFormErrorMetadata = {
    [key: string]: any;
    fieldName: string;
}

export type TTimeGetter = 'getHours' | 'getMinutes';
export type TTimeSetter = 'setHours' | 'setMinutes';


export type TCustomValidators<T, R> = {
    req: (v: T) => Observable<R>,
    handler: (v: R) => ValidationErrors
}