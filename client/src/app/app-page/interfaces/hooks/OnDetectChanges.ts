import { ChangeDetectorRef } from '@angular/core';

export interface OnDetectChanges {
    ngOnDetectChanges: () => void;
}