export interface IOption<T extends object = null> {
    title: string;
    id: number | string;
    icon?: string;
    state?: T;
}