export interface IPage {
    skip: number;
    limit: number;
}