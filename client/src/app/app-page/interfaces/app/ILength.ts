import { ISimple } from './ISimple';

export interface ILength {
    min?: ISimple<number>
    max?: ISimple<number>
};