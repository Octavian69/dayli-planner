import { IOption } from './IOption';
import { ISimple } from './ISimple';

export interface ILinkOption extends IOption {
    link: string;
    query?: ISimple<string>
}