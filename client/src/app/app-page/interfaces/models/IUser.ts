export interface IUser {
    Name: string;
    Login: string;
    Password: string;
    Created: Date;
    _id?: string;
}