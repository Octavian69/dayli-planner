import { trigger, transition, style, animate, AnimationTriggerMetadata, query, animateChild, state, stagger } from "@angular/animations";

export const ANScaleState: (ms?: number) => AnimationTriggerMetadata = (ms: number = 300) => {
   return trigger('ANScaleState', [
        transition('*<=>*', [
            style({
                opacity: 0,
                transform: 'scale(0)'
            }),
            animate(ms, style({ opacity: 1, transform: 'scale(1)', }))
        ])
   ])
}; 

export const ANFade: (ms?: number) => AnimationTriggerMetadata = (ms: number = 400) => {
    return trigger('ANFade', [
        transition(':enter', [
            style({ opacity: 0 }),
            animate(ms, style({
                opacity: 1
            }))
        ]),
        transition(':leave', [
            style({ opacity: 1 }),
            animate(ms, style({ opacity: 0 }))
        ])
    ])
}

export const ANHeight: (ms?: number) => AnimationTriggerMetadata = (ms: number = 300) => {
    return trigger('ANHeight', [
        state('close', style({ height: 0, opacity: 0 })),
        state('open', style({ height: '*', opacity: 1 })),
        transition('close<=>open', animate(ms))
    ])
};


export const ANParent = (ms: number = 0) => trigger('ANParent', [
    transition(':enter', [
        query('@*', [
            stagger(ms, [
                animateChild()
            ])
        ], { optional: true })
    ]),
    transition(':leave', [
        query('@*', [
            animateChild()
        ])
    ])
]);

export const ANParentState = (ms: number = 0) => trigger('ANParentState', [
    transition('*<=>*', [
        query('@*', [
            stagger(ms, [
                animateChild()
            ])
        ], { optional: true })
    ])
]);

export const ANTranslateChildrenY = (
    selector: string = '.an-item', 
    start: string ='-80%', 
    stagerMs: number = 100, 
    childMs: number = 200) => 
        trigger('ANTranslateChildrenY', [
        transition('*<=>*', [
            query(selector, [
                stagger(stagerMs, [
                    style({ opacity: 0, transform: `translateY(${start})` }),
                    animate(childMs, style({ opacity: 1, transform: `translateY(0%)` }))
                ])
            ], { optional: true })
        ])
]);

export const ANShowTranslateY = (ms: number = 400, start: string = '-50%') => trigger('ANShowTranslateY', [
    transition(':enter', [
        style({opacity: 0, transform: `translateY(${start})`}),
        animate(ms, style({opacity: 1, transform: `translateY(0%)`}))
    ]),
    transition(':leave', [
        animate(ms, style({opacity: 0, transform: `translateY(${start})`}))
    ])
]);

export const ANTranslateEnterY = (ms: number = 300, start: string = '80%') => trigger('ANTranslateEnterY', [
    transition(':enter', [
        style({ opacity: 0, transform: `translateY(${start})` }),
        animate(ms, style({ opacity: 1, transform: 'translateY(0%)' }))
    ])
])


export const ANTranslateState = (ms: number = 300) => trigger('ANTranslateState', [
    transition('*<=>*', [
        style({ opacity: 0, transform: 'translateY(-100%)' }),
        animate(ms, style({ opacity: 1, transform: 'translateY(0)' }))
    ])
]);

export const ANShowScale: (ms?: number) => AnimationTriggerMetadata = 
(ms: number = 300) => {
    return trigger('ANShowScale', [
        transition(':enter', [
            style({ transform: 'scale(0)', opacity: 0 }),
            animate(ms, 
                style({ transform: 'scale(1)', opacity: 1 })
            )
        ]),
        transition(':leave', [
            style({ transform: 'scale(1)', opacity: 1 }),
            animate(ms, 
                style({ transform: 'scale(0)', opacity: 0 })
            )
        ]),
    ])
}

