import { Directive, HostListener, Output, Input, EventEmitter, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[dpPageBreak]'
})
export class PageBreakDirective {

  @Input('disabled') disabled: boolean = false;
  @Output() end = new EventEmitter<void>();

  @HostListener('document:scroll', ['$event'])
    scroll(e: MouseWheelEvent) {
      const { scrollTop, scrollHeight, clientHeight } = this.document.documentElement;
      
      const isEnd: boolean = scrollTop >= (scrollHeight - clientHeight);
      const isCanEmit: boolean = isEnd && !this.disabled;

      if(isCanEmit) this.end.emit();
    }

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) { }

}
