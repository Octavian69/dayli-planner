import { Directive, ChangeDetectorRef } from '@angular/core';

@Directive({
  selector: '[dpChangeDetector]'
})
export class ChangeDetectorDirective {

  constructor(
    private cdr: ChangeDetectorRef
  ) { }


  detected(): void {
    this.cdr.detectChanges();
  }

}
