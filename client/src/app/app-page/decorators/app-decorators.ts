export function Bind(): MethodDecorator {
    return (target: any, propName: string, descriptor: PropertyDescriptor) => {
        const value: any = descriptor.value;

        return {
            get() {
                return value.bind(this);
            },
            enumerable: true
        }
    }
}