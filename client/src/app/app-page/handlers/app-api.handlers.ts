export function isCanExpand<T>(data: T[], totalCount: number): boolean {
    return data.length < totalCount;
}