import { TFullDateType, TCompareDateType, TDateMethods, TDateMapMethods } from 'src/app/system-page/dayli-planner-page/types/dayli-planner.types';
import { DBDayliPlanner } from 'src/app/system-page/dayli-planner-page/db/daily-planner.db';

export function prefix(str: string, preff: string): string {
    return `${preff}${str}`;
}

export function suffix(str: string, suff: string): string {
    return `${str}${suff}`;
}


export function deepCopy<T>(data: T): T {
    const dataToString: string = JSON.stringify(data);

    return JSON.parse(dataToString);
}

export function getDataByArrayKeys<T, D extends object>(keys: Array<string>, database: D): T {
    let idx: number = 1;
    let value: T = database[keys[0]];

    while(keys[idx]) {
        const key = keys[idx];
        value = value[key];

        if(!value) {
            value = null;
            break;
        }

        idx++;
    }

    return deepCopy(value as T);
}

export function copyDate(date: Date): Date {
    return new Date(date);
}

export function isLastDateOnMonth(date: Date): boolean {
    const copy: Date = copyDate(date);

    copy.setDate(copy.getDate() + 1);

    return date.getMonth() !== copy.getMonth();
}

export function compareByType(v1: number, v2: number, type: TCompareDateType): boolean {
    switch(type) {
        case 'equal': return v1 === v2;
        case 'before': return v1 < v2;
        case 'after': return v1 > v2;
        case 'less-equal': return v1 <= v2;
        case 'more-equal': return v1 >= v2;
    }
}

export function createMap<K, V>(options: [K, V][]): Map<K, V> {
    return new Map(options);
}

function completeDate(d: Date, depth: TFullDateType): Date {
    if(depth === 'year') return new Date(d.getFullYear(), 0);

    const date: Date = copyDate(d);
    const dateMethods: TDateMethods = getDataByArrayKeys(['dateMethods'], DBDayliPlanner);
    const dateMapMethods: TDateMapMethods = createMap(dateMethods);
    const dateTypes: TFullDateType[] = getDataByArrayKeys(['fullDateTypes'], DBDayliPlanner);
    const depthIdx: number = dateTypes.indexOf(depth) + 1;
    const depthTypes = dateTypes.slice(0, depthIdx);
    const dateParams: number[] = depthTypes.map((depth: TFullDateType) => {
        const [getter] = dateMapMethods.get(depth);

        return date[getter]();
    });

    // @ts-ignore
    return new Date(...dateParams);
     
}

export function compareDates(d1: Date, d2: Date, depth: TFullDateType, compareType: TCompareDateType = 'equal'): boolean {
    if(!d1 || !d2) return false;

    const date1: number = completeDate(d1, depth).getTime();
    const date2: number = completeDate(d2, depth).getTime();

    return compareByType(date1, date2, compareType)

}

export function deepCompare<T>(v1: T, v2: T): boolean {
    return JSON.stringify(v1) === JSON.stringify(v2);
}



