import { TModeType } from 'src/app/app-page/types/app.types';
import { IPNote } from '../interfaces/IPNote';

export class DPNoteState {
    removeNoteCandidate: IPNote = null;
    noteModeType: TModeType = null;
    removeIdsList: string[] = [];
}