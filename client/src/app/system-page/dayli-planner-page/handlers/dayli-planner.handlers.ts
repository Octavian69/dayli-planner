import { TDateType } from '../types/dayli-planner.types';
import { TFromTo } from 'src/app/app-page/types/app.types';
import { copyDate } from 'src/app/app-page/handlers/app-shared.handlers';
import { NSPlannerLimits } from '../namespaces/dayli-planner.namespaces';

export function queryToDate(d: string): Date {
    return new Date(d);
}

export function dateToQuery(date: Date): string {
    const query: string = date.toLocaleDateString()
    .split('.').reverse().join('.');

    return query;
}

export function getDateStringByType(d: Date, type: TDateType): string {

    const [, month, year] = splitDate(d);

    switch(type) {
        case 'year': {
            return String(year);
        }

        case 'month': {
            return `${month}.${year}`
        }

        case 'date': {
            return d.toLocaleDateString();
        }
    }
}

export function convertStringToDate(str: string): Date {
    const date: Date = new Date(str);
    
    if(date instanceof Date) return date;

    throw new Error('Invalid Date Parameter');
}

export function splitDate(d: Date): string[] {
    const date: Date = copyDate(d);

    return date.toLocaleDateString().split('.');
}

export function getDatesPreiodByType(type: TDateType, d: Date): TFromTo<string | number> {
    const [, month, year] = splitDate(d);

    switch(type) {
        case 'year': {
            const from: number = +year;
            const to: number = from + NSPlannerLimits.STATS_YEARS;
            
            return { from, to };
        }
        case 'month': {
            return { from: year }
        }
        case 'date': {
            return { from: `${month}.${year}` }
        }
    }
}