import { TSide } from 'src/app/app-page/types/app.types';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { IPNote } from '../interfaces/IPNote';
import { IPDateStats } from '../interfaces/IPDateStats';
import { DPStatsPeriod } from '../models/DPStatsPeriod';

export type TDateType = 'year' | 'month' | 'date';
export type TFullDateType = TDateType | 'hours' | 'minutes';
export type TDateGetter = 'getDate' | 'getMonth' | 'getFullYear';
export type TDateSetter = 'setDate' | 'setMonth' | 'setFullYear'; 
export type TPlannerStates = Record<TDateType, IPlannerManager>;
export type TModeYearOptions = 'set' | 'complete';
export type TCompareDateType = 'equal' | 'before' | 'after' | 'less-equal' | 'more-equal';
export type TCalendar = Exclude<TDateType, 'date'>
export type TDateMethods = [TFullDateType, [TDateGetter, TDateSetter]][];
export type TDateMapMethods = Map<TFullDateType, [TDateGetter, TDateSetter]>

export type TNoteFormLengthKeys = Pick<IPNote, 'Title' | 'Description'>;

export type TModSettings = {
    type: TModeYearOptions;
    side?: TSide
}

export type TPNoteCandidate = Pick<IPNote, 'Title' | 'Description' | 'Priority' | 'DateString'>;

export type TChangeDate = {
    type: TDateType,
    value: number,
    side?: TSide
};

export type TChangeOptionState = {
    pattern: string
};

export type TUpdateStats = {
    options: IPDateStats[],
    stats: DPStatsPeriod
}

export type TCahceStats = {
    options: IPDateStats[],
    isUpdate: boolean
}