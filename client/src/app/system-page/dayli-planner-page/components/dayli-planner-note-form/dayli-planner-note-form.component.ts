import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { IPNote } from '../../interfaces/IPNote';
import { getDataByArrayKeys } from 'src/app/app-page/handlers/app-shared.handlers';
import { TFormModeType } from 'src/app/app-page/forms/types/app-form.types';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IOption } from 'src/app/app-page/interfaces/app/IOption';
import { DBDayliPlanner } from '../../db/daily-planner.db';
import { ILength } from 'src/app/app-page/interfaces/app/ILength';
import { DPFormLengths } from '../../namespaces/dayli-planner.namespaces';
import { Bind } from 'src/app/app-page/decorators/app-decorators';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { AppValidator } from 'src/app/app-page/forms/models/AppValidator';

@Component({
  selector: 'dp-dayli-planner-note-form',
  templateUrl: './dayli-planner-note-form.component.html',
  styleUrls: ['./dayli-planner-note-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerNoteFormComponent implements OnInit {

  form: FormGroup;
  lengths: ILength = DPFormLengths.Note;
  priorityOptions: IOption[] = getDataByArrayKeys(['priorityOptions'], DBDayliPlanner);
  priorityColors: ISimple<string> = getDataByArrayKeys(['priorityColors'], DBDayliPlanner);

  @Input('request') request: boolean = false;
  @Input('modeType') modeType: TFormModeType = 'create';
  @Input('note') note: IPNote;
  
  @Output('hide') _hide = new EventEmitter<boolean>();
  @Output('submit') _submit = new EventEmitter<IPNote>();

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    const { 
      min: {Title: minTitle},
      max: {Title: maxTitle, Description}
     } = this.lengths;

    this.form = this.fb.group({
      Title: [null, [
        Validators.required, 
        Validators.minLength(minTitle), 
        Validators.maxLength(maxTitle)]],

      Description: [null, [Validators.maxLength(Description)]],
      Priority: [1],
      DateInt: [null, [AppValidator.minTime()]]
    }, { updateOn: "blur" });

    this.form.patchValue(this.note)
  }

  @Bind()
  getOptionsStyle(opt: IOption) {
    return {
      color: this.priorityColors[opt.id]
    }
  }

  hide(): void {
    this._hide.emit(false);
  }

  onSubmit(): void {
    const candidate: IPNote = Object.assign(this.note, this.form.value);
    
    this._submit.emit(candidate);
  }
}
