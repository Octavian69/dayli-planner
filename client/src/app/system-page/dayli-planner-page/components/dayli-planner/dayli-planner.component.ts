import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { DPlannerViewService } from '../../services/dayli-planner.view.service';
import { IOption } from 'src/app/app-page/interfaces/app/IOption';
import { TChangeOptionState, TChangeDate, TDateType, TCalendar } from '../../types/dayli-planner.types';
import { DPlannerFlags } from '../../flags/DPlannerFlags';
import { DPlannerState } from '../../states/dplanner.state';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { dateToQuery, queryToDate } from '../../handlers/dayli-planner.handlers';
import { OnDetectChanges } from 'src/app/app-page/interfaces/hooks/OnDetectChanges';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { IPlannerOption } from '../../interfaces/IPlannerOption';
import { TSide } from 'src/app/app-page/types/app.types';
import { IPlannerManager } from '../../interfaces/IPlannerManager';
import { ANParent, ANShowTranslateY } from 'src/app/app-page/animations/animations';

@UntilDestroy()
@Component({
  selector: 'dp-dayli-planner',
  templateUrl: './dayli-planner.component.html',
  styleUrls: ['./dayli-planner.component.scss'],
  animations: [ANParent(), ANShowTranslateY()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerComponent implements OnInit, OnInitMetatdata, OnDetectChanges, OnDestroy {

  date$: Observable<Date> = this.getDateStream();

  flags: DPlannerFlags = this.viewService.getFlags();
  state: DPlannerState = this.viewService.getState();
  weekdays: string[] = this.viewService.getDataByArrayKeys(['weekdays']);
  options: IOption<TChangeOptionState>[] = this.viewService.getDataByArrayKeys(['changeDateList']);

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    public cdr: ChangeDetectorRef,
    private viewService: DPlannerViewService,
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnInitMetatdata(): void {
    this.flags = this.viewService.getFlags();
    this.state = this.viewService.getState();
  }

  ngOnDetectChanges(): void {
    this.viewService.detector$
    .pipe(untilDestroyed(this))
    .subscribe(_ => {
      this.cdr.detectChanges();
    })
  }

  ngOnDestroy(): void {}

  init(): void {
    this.ngOnInitMetatdata();
    this.ngOnDetectChanges();
    this.initStats();
  }

  initStats(): void {
    const dateParam: string = this.activeRoute.snapshot.queryParamMap.get('date');
    const date: Date = queryToDate(dateParam);
    
    this.viewService.getStats('date', date);
  }

  getDateStream(): Observable<Date> {
    return this.viewService.getStream('date$').pipe(
      tap((date: Date) => {
        if(date instanceof Date) {
          const queryParams: Params = { date: dateToQuery(date) };
          this.router.navigate([], {
            queryParams,
            relativeTo: this.activeRoute
          });
        }
      })
    )
  }

  showPanel(flag: boolean): void {
    this.viewService.setFlag('isShowChangePanel', flag);
  }

  openState(type: TCalendar): void {
    this.viewService.openCalendar(type);
  }

  step(event: TChangeDate): void {
    this.viewService.step(event);
  }

  change(opt: IPlannerOption): void {
    this.viewService.change(opt);
  }

  getOptions(type: TDateType): IPlannerOption[] | IPlannerOption[][] {
    return this.viewService.getOptions(type);
  }

  resetCurrentState(): void {
    this.viewService.closeCalendar();
  }

  changePeriod(side: TSide): void {
    this.viewService.changePeriod(side);
  }

  setCurrentState(type: TCalendar): void {
    this.viewService.setCurrentState(type);
  }

  getCurrentOptions(): IPlannerOption[] {
    return this.viewService.getCurrentOptions();
  }

  getCurrentState(): IPlannerManager {
    return this.viewService.getCurrentState();
  }

  getCurrentStateType(): TCalendar {
    return this.viewService.getCurrentStateType();
  }

  getMinDate(): Date {
    return this.viewService.getMinDate();
  }
}
