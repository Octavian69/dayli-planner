import { Component, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { DPNote } from '../../models/DPNote';
import { IPNote } from '../../interfaces/IPNote';
import { TModeType } from 'src/app/app-page/types/app.types';

@Component({
  selector: 'dp-dayli-planner-mode-note',
  templateUrl: './dayli-planner-mode-note.component.html',
  styleUrls: ['./dayli-planner-mode-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerModeNoteComponent {

  @Input() note: IPNote;
  @Input() modeType: TModeType;
  @Input('request') request: boolean = false;
  @Input('date') currentDate: Date;
  @Output('hide') _hide = new EventEmitter<boolean>(); 
  @Output('create') _create = new EventEmitter<IPNote>(); 
  @Output('edit') _edit = new EventEmitter<IPNote>(); 

  hide(): void {
    this._hide.emit(false)
  }

  getTitle(): string {
    const action: string = this.modeType === 'create' ? 'Создание' : 'Редактирование';

    return `${action} заметки`
  }

  submit(note: IPNote): void {
    this.modeType === 'create'
     ? this._create.emit(note)
     : this._edit.emit(note)
  }
}
