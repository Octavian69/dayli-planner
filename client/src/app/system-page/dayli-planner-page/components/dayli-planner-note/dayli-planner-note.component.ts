import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { IPNote } from '../../interfaces/IPNote';
import { TShowStatus } from 'src/app/app-page/types/app.types';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { ANHeight } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-dayli-planner-note',
  templateUrl: './dayli-planner-note.component.html',
  styleUrls: ['./dayli-planner-note.component.scss'],
  animations: [ANHeight()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerNoteComponent {

  public status: TShowStatus = 'close';

  @Input() note: IPNote;
  @Input() disabled: boolean = false;
  @Input() removed: boolean = false;
  @Input() priorityTitle: string;
  @Input() priorityColor: string;

  @Output('edit') _edit = new EventEmitter<IPNote>();
  @Output('remove') _remove = new EventEmitter<IPNote>();

  getArrowStyle(): ISimple<string> {
    const deg: number = this.status === 'open' ? 180 : 0;

    return {
      transform: `rotate(${deg}deg)`,
      transition: 'transform 0.3s linear'
    }
  }

  getBorderColor(): string {
    const color: string = this.isOpen() ? 'orange' : this.disabled ? 'darkgray' : this.priorityColor;

    return color;
  }

  getDescriptionStyles(): ISimple<string> {
    const isExist: boolean = Boolean(this.note.Description);

    return {
      color: isExist ? 'darkmagenta' : 'darkgray'
    }
  }

  isOpen(): boolean {
    return this.status === 'open';
  }

  edit(): void {
    this._edit.emit(this.note);
  }

  remove(): void {
    this._remove.emit(this.note);
  }

  open(): void {
    this.status = this.status === 'close' ? 'open' : 'close';
  }

}
