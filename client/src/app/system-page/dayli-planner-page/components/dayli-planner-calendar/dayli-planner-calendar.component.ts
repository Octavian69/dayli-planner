import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { ANHeight } from 'src/app/app-page/animations/animations';
import { TCalendar, TDateType } from '../../types/dayli-planner.types';
import { IPlannerOption } from '../../interfaces/IPlannerOption';
import { TSide } from 'src/app/app-page/types/app.types';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { getDataByArrayKeys } from 'src/app/app-page/handlers/app-shared.handlers';
import { DBDayliPlanner } from '../../db/daily-planner.db';

@Component({
  selector: 'dp-dayli-planner-calendar',
  templateUrl: './dayli-planner-calendar.component.html',
  styleUrls: ['./dayli-planner-calendar.component.scss'],
  animations: [ANHeight()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerCalendarComponent {

  dateTitles: Record<TDateType, string> = getDataByArrayKeys(['dateTitles'], DBDayliPlanner);

  @Input('type') type: TCalendar = 'year';
  @Input('minDate') minDate: Date;
  @Input() options: IPlannerOption[] = [];
  @Output('hide') _hide = new EventEmitter<void>();
  @Output('change') _change = new EventEmitter<IPlannerOption>();
  @Output('setState') _setState = new EventEmitter<TCalendar>();
  @Output('changePeriod') _changePeriod = new EventEmitter<TSide>();

  constructor(
    private sanitaizer: DomSanitizer
  ) {}

  trackByFn(idx: number, opt: IPlannerOption): number {
    return opt.value;
  }

  getNavClass(): ISimple<boolean> {
    return {
      'year': this.type === 'year',
      'month': this.type === 'month',
    }
  }
  
  getStateHtml(): SafeHtml {
    const stateTitle: string = this.type === 'year' ? 'Месяц' : 'Год';
    const stateText: string = `Выбрать ${stateTitle}`;
    const icon: string = this.type === 'year' ? 'arrow_forward' : 'arrow_back'
    const stateIcon: string = `<i class="material-icons change-icon">${icon}</i>`
    const html: string = this.type === 'year' ? stateText + stateIcon : stateIcon + stateText;
    
    return this.sanitaizer.bypassSecurityTrustHtml(html);
  }

  getTooltip(opt: IPlannerOption): string {
    const dateTitle: string = opt.type === 'year' ? 'Год' : 'Месяц';

    switch(true) {
      case opt.current: {
        return `Текущий ${dateTitle}`;
      }
      case opt.selected: {
        return `Выбранный ${dateTitle}`;
      }
      case opt.disabled: {
        const dateString: string = this.minDate.toLocaleString();

        return `Дата не может быть раньше регистрационной.\n${dateString}`;
      }
      default: {
        return `Выбрать ${dateTitle}`
      }
    }
  }

  getBadgeTooltip(notesCount: number): string {
    const title: string = this.dateTitles[this.type];
     
    return `Заметок за ${title}: ${notesCount}`;
  }
  
  setState(): void {
    const type: TCalendar = this.type === 'year' ? 'month' : 'year';

    this._setState.emit(type);
  }

  change(opt: IPlannerOption): void {
    if(!opt.disabled) {
      this._change.emit(opt);
    }
  }
  
  changePeriod(side: TSide): void {
    this._changePeriod.emit(side);
  }

  hide(): void {
    this._hide.emit()
  }

  isDisabled(): boolean {
    return this.options[0]?.value <= this.minDate.getFullYear();
  }
}
