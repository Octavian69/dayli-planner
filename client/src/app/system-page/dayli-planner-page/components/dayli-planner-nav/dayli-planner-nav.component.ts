import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DPlannerViewService } from '../../services/dayli-planner.view.service';
import { ILinkOption } from 'src/app/app-page/interfaces/app/ILinkOption';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { DPlannerState } from '../../states/dplanner.state';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter } from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'dp-dayli-planner-nav',
  templateUrl: './dayli-planner-nav.component.html',
  styleUrls: ['./dayli-planner-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerNavComponent implements OnInit, OnInitMetatdata, OnDestroy {

  currentUrl: string;
  state: DPlannerState;
  options: ILinkOption[];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: DPlannerViewService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy() {}

  ngOnInitMetatdata(): void {
    this.options = this.viewService.getDataByArrayKeys(['navList']);
    this.state = this.viewService.getState();
  }

  init(): void {
    this.ngOnInitMetatdata();
    this.initRouterSub();
  }

  initRouterSub(): void {
    this.currentUrl = this.router.url.split('?')[0];

    this.router.events.pipe(
      filter((e) => e instanceof NavigationEnd),
      untilDestroyed(this)
    ).subscribe((e: NavigationEnd) => {
      this.currentUrl = e.url.split('?')[0];
      this.cdr.detectChanges();
    })
  }

  isCurrentPage(link: string): boolean {
    return link === this.currentUrl;
  }

  trackByFn(idx: number) {
    return idx;
  }

  navigate(opt: ILinkOption) {
    const isCurrent: boolean = this.isCurrentPage(opt.link);

    if(isCurrent) return;

    const { snapshot: { queryParams } } = this.activeRoute;
    const { link } = opt;

    this.router.navigate([link], { queryParams });
  }

}
