import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IPlannerOption } from '../../interfaces/IPlannerOption';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';

@Component({
  selector: 'dp-dayli-planner-dates-list',
  templateUrl: './dayli-planner-dates-list.component.html',
  styleUrls: ['./dayli-planner-dates-list.component.scss']
})
export class DayliPlannerDatesListComponent {

  @Input('weeks') weeks: IPlannerOption[][] = [];
  @Input('loading') loading: boolean = false;
  @Output('change') _change = new EventEmitter<IPlannerOption>()


  getDateClasses(opt: IPlannerOption): ISimple<boolean> {
    const { disabled, current, selected } = opt;

    return {disabled, current, selected }
  }

  getTooltips(opt: IPlannerOption) {
    const { current, selected, disabled } = opt;

    switch(true) {
      case current: {
        return `Текущая дата`;
      }
      case selected: {
        return `Выбранная дата`;
      }

      case disabled: {
        return '';
      }

      default: {
        return 'Выбрать дату';
      }
    }
  }

  getBadgeTooltip(notesCount: number): string {
    return `Заметок: ${notesCount}`;
  }

  change(opt: IPlannerOption): void {
    const { disabled, selected } = opt;

    if(!disabled && !selected ) {
      this._change.emit(opt);
    }
  }

}
