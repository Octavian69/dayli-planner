import { Component, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { TSide } from 'src/app/app-page/types/app.types';
import { TChangeDate, TDateType, TChangeOptionState, TCalendar } from '../../types/dayli-planner.types';
import { IOption } from 'src/app/app-page/interfaces/app/IOption';
import { compareDates } from 'src/app/app-page/handlers/app-shared.handlers';
import { ANTranslateState } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-dayli-planner-change-panel',
  templateUrl: './dayli-planner-change-panel.component.html',
  styleUrls: ['./dayli-planner-change-panel.component.scss'],
  animations: [ANTranslateState()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerChangePanelComponent {

  animationState: boolean = false;
  animationType: TDateType = null;

  @Input() options: IOption<TChangeOptionState>[] = [];
  @Input() show: boolean = false;
  @Input() disable: TDateType = null;
  @Input('date') date: Date = new Date();
  @Input('minDate') minDate: Date;
  @Output('step') _step = new EventEmitter<TChangeDate>();
  @Output('showPanel') _showPanel = new EventEmitter<boolean>();
  @Output('openState') _openState = new EventEmitter<TCalendar>();

  constructor() { }

  step(side: TSide, type: TDateType) {
    const isDisabled: boolean = type === this.disable;
    if(isDisabled) return;
    const value: number = side === 'prev' ? -1 : 1;
    const option: TChangeDate = { type, value };
    this.animationTrigger(type);

    this._step.emit(option);
  }

  animationTrigger(type: TDateType): void {
    this.animationState = !this.animationState;
    this.animationType = type;
  }

  trackByFn(idx: number): number {
    return idx;
  }

  openState(type: TDateType): void {
    if(type !== 'date') this._openState.next(type);
  }

  showPanel(): void {
    this._showPanel.emit(!this.show);
  }

  isDisabled(type: TDateType): boolean {
    return compareDates(this.date, this.minDate, type, 'less-equal');
  }

  getPanelTooltip(): string {
    const action: string = this.show ? 'Скрыть' : 'Показать';

    return `${action} панель.`
  }

  getArrowTooltip(type: TDateType): string {
    const isDisabled: boolean = this.isDisabled(type);
    const message: string = isDisabled 
    ? `Вы не можете выбрать дату раньше регистрационной: ${this.minDate.toLocaleDateString()}`
    : '';

    return message;
  }

}
