import { Component, OnInit } from '@angular/core';
import { DPlannerViewService } from '../../services/dayli-planner.view.service';
import { ActivatedRoute } from '@angular/router';
import { convertStringToDate } from '../../handlers/dayli-planner.handlers';

@Component({
  selector: 'dp-dayli-planner-page',
  templateUrl: './dayli-planner-page.component.html',
  styleUrls: ['./dayli-planner-page.component.scss']
})
export class DayliPlannerPageComponent implements OnInit {

  constructor(
    private dpViewService: DPlannerViewService,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.dpViewService.reset();
  }

  init(): void {
    const dateStr: string = this.activeRoute.snapshot.queryParamMap.get('date');
    const date: Date = dateStr ? convertStringToDate(dateStr) : new Date();

    this.dpViewService.init(date);
  }
}
