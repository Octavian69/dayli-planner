import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, HostBinding } from '@angular/core';
import { IPNote } from '../../interfaces/IPNote';
import { DPNotesViewService } from '../../services/dp-notes.view.service';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { DPNotesFlags } from '../../flags/DPNotesFlags';
import { Observable } from 'rxjs';
import { OnDetectChanges } from 'src/app/app-page/interfaces/hooks/OnDetectChanges';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { TPaginationRes, TListAction } from 'src/app/app-page/types/app-api.types';
import { Page } from 'src/app/app-page/classes/Page';
import { IPage } from 'src/app/app-page/interfaces/app/IPage';
import { NSPlannerLimits } from '../../namespaces/dayli-planner.namespaces';
import { ActivatedRoute } from '@angular/router';
import { queryToDate } from '../../handlers/dayli-planner.handlers';
import { DPNoteState } from '../../states/dpnote.state';
import { TModeType } from 'src/app/app-page/types/app.types';
import { DPNote } from '../../models/DPNote';
import { isCanExpand } from 'src/app/app-page/handlers/app-api.handlers';
import { compareDates } from 'src/app/app-page/handlers/app-shared.handlers';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { ANParent, ANShowTranslateY } from 'src/app/app-page/animations/animations';

@UntilDestroy()
@Component({
  selector: 'dp-dayli-planner-notes',
  templateUrl: './dayli-planner-notes.component.html',
  styleUrls: ['./dayli-planner-notes.component.scss'],
  animations: [ANParent(100), ANShowTranslateY()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayliPlannerNotesComponent implements OnInit, OnInitMetatdata, OnDetectChanges, OnDestroy {

  notes$: Observable<TPaginationRes<IPNote>> = this.viewService.getStream('notes$');
  modeNote$: Observable<IPNote> = this.viewService.getStream('modeNote$');

  currentDate: Date;
  DateString: string;
  page: IPage;

  flags: DPNotesFlags;
  state: DPNoteState;
  priorityTitles: ISimple<string>;
  priorityColors: ISimple<string>;
  notes: IPNote[] = [];
  disabledCreate: boolean;

  constructor(
    private activeRoute: ActivatedRoute,
    private viewService: DPNotesViewService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.ngOnInitMetatdata();
    this.ngOnDetectChanges();
    this.fetch();
  }

  ngOnDestroy() {
    this.viewService.reset(new DPNotesFlags);
  }

  ngOnInitMetatdata(): void {
    const paramDate: string = this.activeRoute.snapshot.queryParamMap.get('date');

    this.flags = this.viewService.getFlags();
    this.state = this.viewService.getState();
    // this.page = new Page(NSPlannerLimits.NOTES);
    this.page = new Page(1);
    this.priorityTitles = this.viewService.getDataByArrayKeys(['priorityTitles']);
    this.priorityColors = this.viewService.getDataByArrayKeys(['priorityColors']);
    
    this.currentDate = queryToDate(paramDate);
    this.DateString = this.currentDate.toLocaleDateString();
    this.disabledCreate = compareDates(this.currentDate, new Date, 'date', 'before');
  }

  ngOnDetectChanges() {
    this.viewService.getStream('detector$').pipe(
      untilDestroyed(this)
    ).subscribe(_ => {
      this.cdr.detectChanges();
    })
  }

  getNoteSettings(key: 'title' | 'color', value: number): string {
    const obj: ISimple<string> = key === 'title' ? this.priorityTitles : this.priorityColors;

    return obj[value];
  }

  hideModeModal(): void {
    this.viewService.hideModeModal();
  }

  modificateNote(note: IPNote, modeType: TModeType): void {
    this.viewService.setStateValue('noteModeType', modeType);

    note = note || new DPNote(this.currentDate);

    this.viewService.emitStream('modeNote$', note);
    
  }

  getCreateTooltip(): string {
    return this.disabledCreate
     ? `Вы не можете создать заметку, раньше текущей даты: ${new Date().toLocaleDateString()}` 
     : 'Создать заявку'
  }

  trackByFn(idx: number, note: IPNote): string {
    return note._id;
  }

  getRemoveConfirmText(note: IPNote): string {
    return `удалить заметку: "${note.Title}"`;
  }

  addRemoveCandidate(note: IPNote): void {
    this.viewService.setStateValue('removeNoteCandidate', note);
  }

  fetch(action: TListAction = 'replace'): void {
    this.viewService.fetch(this.DateString, this.page, action);
  }

  create(note: IPNote): void {
    this.viewService.create(note)
  }

  edit(note: IPNote): void {
    this.viewService.edit(note);
  }

  remove(confirm: boolean): void {
    if(confirm) {
      const note: IPNote = this.viewService.getStateValue('removeNoteCandidate');
      this.viewService.remove(note);
    } 
    else this.viewService.setStateValue('removeNoteCandidate', null); 
    
  }

  expand(): void {
    const { rows, totalCount }: TPaginationRes<IPNote> = this.viewService.getStreamValue('notes$');
    const isExpand: boolean = isCanExpand(rows, totalCount);

    if(isExpand) {
      this.page.skip++;
      this.fetch('expand');
    }
  }
}
