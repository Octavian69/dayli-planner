import { TDateType } from '../types/dayli-planner.types';
import { PlannerStatesManager } from '../managers/PlannerStatesManager';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { YearManager } from '../managers/YearManager';
import { MonthManager } from '../managers/MonthManager';
import { DateManager } from '../managers/DateManager';

export class DateStatesFactory {
    constructor() {}

    create(type: TDateType, date: Date, manager: PlannerStatesManager): IPlannerManager {
        switch(type) {
            case 'year': {
                const value: number = date.getFullYear();
                
                return new YearManager(type, value, manager);
            }
            case 'month': {
                const value: number = date.getMonth();

                return new MonthManager(type, value, manager)
            }
            case 'date': {
                const value: number = date.getDate();

                return new DateManager(type, value, manager);
            }
        }
    }
}