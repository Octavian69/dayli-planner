import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/app-page/modules/core.module';
import { MaterialModule } from 'src/app/app-page/modules/material.module';
import { DayliPlannerRoutingModule } from './dayli-planner.routing.module';
import { ComponentsModule } from 'src/app/app-page/modules/components.module';

import { DPlannerViewService } from './services/dayli-planner.view.service';

import { DayliPlannerComponent } from './components/dayli-planner/dayli-planner.component';
import { DayliPlannerPageComponent } from './components/dayli-planner-page/dayli-planner-page.component';
import { DayliPlannerNavComponent } from './components/dayli-planner-nav/dayli-planner-nav.component';
import { DayliPlannerChangePanelComponent } from './components/dayli-planner-change-panel/dayli-planner-change-panel.component';
import { DayliPlannerDatesListComponent } from './components/dayli-planner-dates-list/dayli-planner-dates-list.component';
import { DayliPlannerCalendarComponent } from './components/dayli-planner-calendar/dayli-planner-calendar.component';
import { DayliPlannerNotesComponent } from './components/dayli-planner-notes/dayli-planner-notes.component';
import { DayliPlannerNoteFormComponent } from './components/dayli-planner-note-form/dayli-planner-note-form.component';
import { DayliPlannerNoteComponent } from './components/dayli-planner-note/dayli-planner-note.component';
import { DPNotesViewService } from './services/dp-notes.view.service';
import { DayliPlannerModeNoteComponent } from './components/dayli-planner-mod-note/dayli-planner-mode-note.component';
import { DirectivesModule } from 'src/app/app-page/modules/directives.module';

@NgModule({
    declarations: [
        DayliPlannerPageComponent,
        DayliPlannerComponent,
        DayliPlannerNavComponent,
        DayliPlannerChangePanelComponent,
        DayliPlannerDatesListComponent,
        DayliPlannerCalendarComponent,
        DayliPlannerNotesComponent,
        DayliPlannerNoteComponent,
        DayliPlannerNoteFormComponent,
        DayliPlannerModeNoteComponent
    ],
    imports: [
        CoreModule,
        MaterialModule,
        ComponentsModule,
        DirectivesModule,
        DayliPlannerRoutingModule
    ],
    providers: [
        DPlannerViewService,
        DPNotesViewService
    ]
})
export class DayliPlannerModule {}