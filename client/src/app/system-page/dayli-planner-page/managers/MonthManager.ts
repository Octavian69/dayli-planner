import { TChangeDate, TUpdateStats } from '../types/dayli-planner.types';
import { PlannerManager } from './PlannerManager';
import { copyDate, getDataByArrayKeys, compareDates } from 'src/app/app-page/handlers/app-shared.handlers';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { IPlannerOption } from '../interfaces/IPlannerOption';
import { DBDayliPlanner } from '../db/daily-planner.db';
import { getDateStringByType } from '../handlers/dayli-planner.handlers';

export class MonthManager extends PlannerManager<IPlannerOption> implements IPlannerManager {
    static MONTHS: string[] = getDataByArrayKeys(['months'], DBDayliPlanner);

    options: IPlannerOption[] = [];

    setValue(date: Date): void {
        this.value = date.getMonth();
    }

    change(opt: IPlannerOption): Date {
        let date: Date = copyDate(this.stateManager.getDate());
        const { value } = opt;

        date.setMonth(value);
        
        const month: number = date.getMonth();
        const isNoteEqualMonth: boolean = month !== value;
        
        if(isNoteEqualMonth)  date.setDate(0);

        date = this.stateManager.checkedDate(date);
        
        this.setValue(date);
        this.stateManager.resetCurrentState();

        return date;
    }

    step(event: TChangeDate): Date {
        let date: Date = copyDate(this.stateManager.getDate());
        const currentDate: Date = copyDate(date);
        const step: number = event.value;
        const isDecember: boolean = currentDate.getMonth() === 11;
        const isJunaury: boolean = currentDate.getMonth() === 0;
        const isNextYear: boolean = isDecember && step === 1;
        const isPrevYear: boolean = isJunaury && step === -1;
        const currentMonth: number = isNextYear
            ? -1
            : isPrevYear
            ? 12
            : currentDate.getMonth();

        currentDate.setDate(1);
        date.setMonth(date.getMonth() + step);
        
        const isNoteEqualMonth: boolean = (currentMonth + step) === date.getMonth();

        if(!isNoteEqualMonth) date.setDate(0);

        date = this.stateManager.checkedDate(date);

        this.setValue(date);
         
        return date;
    }

    completeOptions(): void {
        const date: Date = copyDate(this.stateManager.getDate());
        const currentDate: Date = copyDate(date);
        const minDate: Date = this.stateManager.getMinDate();
        const now: Date = new Date();
 
        this.options = MonthManager.MONTHS.map((title: string, i: number) => {
            date.setMonth(i)

            const disabled: boolean = compareDates(date, minDate, 'month', 'before');
            const current: boolean = compareDates(date, now, 'month', 'equal');
            const selected: boolean = compareDates(date, currentDate, 'month', 'equal');
            const dateString: string = getDateStringByType(date, 'month');
            const value = i;

            return { 
                title,
                disabled,
                current,
                selected,
                value,
                type: this.type,
                dateString,
                stats: null
             }
        })
    }

    updateOptions(data: TUpdateStats): void {
        this.options = super.update(this.options, data.options);
    }
}