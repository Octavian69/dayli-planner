import { TDateType } from '../types/dayli-planner.types';
import { PlannerStatesManager } from './PlannerStatesManager';
import { IPlannerOption } from '../interfaces/IPlannerOption';
import { IPDateStats } from '../interfaces/IPDateStats';

export class PlannerManager<T> {

    options: T[] = [];

    constructor(
        public type: TDateType, 
        public value: number, 
        public stateManager: PlannerStatesManager) {
            this.init();
    }

    init(): void {
        this.completeOptions();
    }

    getValue(): number {
        return this.value;
    }

    getOptions() {
        return this.options;
    }

    setValue(date: Date): void {}
    completeOptions(): void {}

    update(options: IPlannerOption[], statsOptions: IPDateStats[]): IPlannerOption[] {
        return options.map((day: IPlannerOption) => {
            const stats: IPDateStats = statsOptions.find(s => s.DateString === day.dateString);

            day.stats = stats || null;

            return day;
        })
    }
}