import { TChangeDate, TModSettings, TUpdateStats } from '../types/dayli-planner.types';
import { PlannerManager } from './PlannerManager';
import { copyDate } from 'src/app/app-page/handlers/app-shared.handlers';
import { IPlannerOption } from '../interfaces/IPlannerOption';
import { NSPlannerLimits } from '../namespaces/dayli-planner.namespaces';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { IPDateStats } from '../interfaces/IPDateStats';
import { getDateStringByType } from '../handlers/dayli-planner.handlers';


export class YearManager extends PlannerManager<IPlannerOption> implements IPlannerManager {
    options: IPlannerOption[] = [];

    setValue(date: Date) {
        this.value = date.getFullYear();
    }

    private set(value: number): Date {
        let date: Date = copyDate(this.stateManager.getDate());
        const currentDate: Date = copyDate(date);

        date.setFullYear(value);

        const isNotEqualMonths: boolean = date.getMonth() !== currentDate.getMonth();

        if(isNotEqualMonths) date.setDate(0);

        date = this.stateManager.checkedDate(date);

        this.setValue(date)

        return date;
    }

    change(opt: IPlannerOption): Date {
        const date: Date = this.set(opt.value);
        
        this.stateManager.getState('month').setValue(date);
        this.stateManager.setCurrentState('month');

        return date;
    }

    step(event: TChangeDate): Date {
        const value: number = this.value + event.value;
        const date: Date = this.set(value);

        return date;
    }

    completeOptions(settings: TModSettings = { type: 'complete' }): void {
        const date: Date = this.stateManager.getDate();
        const minDate: Date = this.stateManager.getMinDate();
        const currentYear: number = new Date().getFullYear();
        const selectedYear: number = date.getFullYear();
        const minYear: number = minDate.getFullYear();
        const limit: number = NSPlannerLimits.STATS_YEARS;

        let start: number;
        let end: number;

        switch(settings.type) {
            case 'complete': {
                start = date.getFullYear();
                end = start + limit;
                break;
            }

            case 'set': {
                const { side } = settings;
                const { value } = this.options[0];
                start = side === 'next' ? value + limit : value - limit;
                end = start + limit;
                break;
            }
        }

        const options: IPlannerOption[] = [];

        for(let i = start; i < end; i++) {
            const value: number = i;
            const current: boolean = i === currentYear;
            const disabled: boolean = i < minYear;
            const selected: boolean = i === selectedYear;
            const dateString: string = String(i);

            const option: IPlannerOption = {
                value,
                current,
                disabled,
                selected,
                dateString,
                type: this.type,
                stats: null
            }
            
            options.push(option);
        }
        
        this.options = options;
    }

    updateOptions(data: TUpdateStats): void {
       this.options = super.update(this.options, data.options);
    }
}