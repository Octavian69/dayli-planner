import { TChangeDate, TDateType, TPlannerStates, TModSettings, TUpdateStats } from '../types/dayli-planner.types';
import { IPlannerStatesManager } from '../interfaces/IPlannerStatesManager';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { DateStatesFactory } from '../classes/DateStatesFactory';
import { compareDates, copyDate } from 'src/app/app-page/handlers/app-shared.handlers';
import { IPlannerOption } from '../interfaces/IPlannerOption';

export class PlannerStatesManager implements IPlannerStatesManager {

    currentState: IPlannerManager;
    states: TPlannerStates = {} as TPlannerStates;

    constructor(
        public date: Date,
        public minDate: Date,
        states: TDateType[]
    ) {
        
        this.init(states);
    }

    init(states: TDateType[]): void {
        const factory: DateStatesFactory = new DateStatesFactory();

        states.forEach((type: TDateType) => {
            this.states[type] = factory.create(type, this.date, this);
        })
    }

    checkedDate(date: Date) {
        const isLess: boolean = date.getTime() < this.minDate.getTime();

        return isLess ? copyDate(this.minDate) : date;
    }

    getMinDate(): Date {
        return this.minDate
    }

    setMinDate(d: Date): void {
        const date: Date = copyDate(d);

        this.minDate = date;
    }

    updateStatesValues(): void {
        Object.keys(this.states).forEach((stateKey: TDateType) => {
            this.states[stateKey].setValue(this.date);
        })
    }

    completeOptions(type: TDateType, settings?: TModSettings) {
        this.states[type].completeOptions(settings);
    }
    
    completeStatesOptions(): void {
        Object.keys(this.states).forEach((stateKey: TDateType) => {
            this.states[stateKey].completeOptions();
        })
    }

    change(opt: IPlannerOption): Date {
        this.date = this.states[opt.type].change(opt);

        return this.date;
    }

    setFullDate(date: Date): Date {
        this.date = date;
        this.updateStatesValues();

        return this.date;
    }

    step(event: TChangeDate): Date {
        this.date = this.states[event.type].step(event);

        return this.date;
    }

    isChangePeriod(prevDate: Date): boolean {
        const isCurrentPeriod: boolean = compareDates(this.date, prevDate, 'month');
        
        return isCurrentPeriod ? false : true;
    }

    updateOptions(data: TUpdateStats): void {
        this.getState(data.stats.Type).updateOptions(data);
    }

    getDate(): Date {
        return this.date;
    }

    getState(key: TDateType): IPlannerManager {
        return this.states[key];
    }

    getStateValue(key: TDateType): number {
        return this.states[key].getValue();
    }

    getCurrentState(): IPlannerManager {
        return this.currentState;
    }

    setCurrentState(key: TDateType): void {
        this.states[key].completeOptions();
        this.currentState = this.states[key];
    }

    resetCurrentState(): void {
        this.currentState = null;
    }

    getOptionsByType<T>(type: TDateType): T[] {
        return this.states[type].getOptions();
    }
}