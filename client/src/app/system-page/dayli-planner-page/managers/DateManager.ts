import { TChangeDate, TUpdateStats } from '../types/dayli-planner.types';
import { PlannerManager } from './PlannerManager';
import { copyDate, compareDates } from 'src/app/app-page/handlers/app-shared.handlers';
import { IPlannerOption } from '../interfaces/IPlannerOption';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { getDateStringByType } from '../handlers/dayli-planner.handlers';

export class DateManager extends PlannerManager<IPlannerOption[]> implements IPlannerManager {
    options: IPlannerOption[][];

    setValue(date: Date): void {
        this.value = date.getDate();
    }

    change(opt: IPlannerOption): Date { 
        let date: Date = copyDate(this.stateManager.getDate());
        const { value } = opt;

        date.setDate(value);
        date = this.stateManager.checkedDate(date);
        this.setValue(date);

        return date;
    }

    step(event: TChangeDate): Date {
        let date: Date = copyDate(this.stateManager.getDate());
        
        date.setDate(date.getDate() + event.value);
        date = this.stateManager.checkedDate(date);
        
        this.setValue(date);
        
        return date;
    }

    completeOptions(): void {
        const date: Date = copyDate(this.stateManager.getDate());
        const currentDate: Date = copyDate(date);
        const now: Date = new Date();
        const minDate: Date = this.stateManager.getMinDate();
        const weeks: IPlannerOption[][] = new Array(6).fill('').map(_ => ([]));

        date.setDate(1);

        const dayOfWeek: number = date.getDay() || 7;
        const lastWeek: IPlannerOption[] = weeks[weeks.length - 1];
        let weekIdx: number = 0;

        if(dayOfWeek !== 1) { //если первый день, не понедельник
            date.setDate(date.getDate() - dayOfWeek + 1);
        }

        while(lastWeek.length !== 7) {

            const value: number = date.getDate();
            const dateString: string = getDateStringByType(date, 'date');
            const disabled: boolean = 
                    compareDates(date, minDate, 'date', 'before')
                 || !compareDates(date, currentDate, 'month', 'equal')
            const current: boolean = compareDates(date, now, 'date') && !disabled;
            const selected: boolean = compareDates(date, currentDate, 'date', 'equal') && !disabled;

            const option: IPlannerOption = {
                type: this.type,
                value,
                disabled,
                selected,
                current,
                dateString,
                stats: null
            }

            weeks[weekIdx].push(option);

            date.setDate(value + 1);

            const isLastDay: boolean = weeks[weekIdx].length === 7;
            if(isLastDay) weekIdx++;
        }


        this.options = weeks;
    }

    refreshSelectedOptions(): void {
        this.options.forEach(week => {
            week.forEach(day => {
                const { value, disabled } = day;
                const isSelected: boolean = value === this.value && !disabled;
                
                day.selected = isSelected;
            })
        })
    }

    updateOptions(data: TUpdateStats): void {
       this.options = this.options.map((week: IPlannerOption[]) => {
           return super.update(week, data.options);
        });
    }

}