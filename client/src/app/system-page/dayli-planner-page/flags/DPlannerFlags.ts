export class DPlannerFlags {
    isShowChangePanel: boolean = true;
    isShowCalendar: boolean = false;
    isSetDate: boolean = false;

    constructor() {}
}