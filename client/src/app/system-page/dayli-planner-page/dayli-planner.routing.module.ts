import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DayliPlannerPageComponent } from './components/dayli-planner-page/dayli-planner-page.component';
import { DayliPlannerComponent } from './components/dayli-planner/dayli-planner.component';
import { DayliPlannerNotesComponent } from './components/dayli-planner-notes/dayli-planner-notes.component';

const routes: Routes = [
    { path: '', component: DayliPlannerPageComponent, children: [
        { path: '', component: DayliPlannerComponent },
        { path: 'notes', component: DayliPlannerNotesComponent },
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DayliPlannerRoutingModule {}