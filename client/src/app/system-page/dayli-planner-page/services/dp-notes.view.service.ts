import { Injectable } from '@angular/core';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';
import { DPNotesFlags } from '../flags/DPNotesFlags';
import { DBDayliPlanner } from '../db/daily-planner.db';
import { IPNote } from '../interfaces/IPNote';
import { DayliPlannerService } from './dayli-planner.service';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { TPaginationRes, TListAction, TMessageRes } from 'src/app/app-page/types/app-api.types';
import { IPage } from 'src/app/app-page/interfaces/app/IPage';
import { takeUntil, delay } from 'rxjs/operators';
import { DPNoteState } from '../states/dpnote.state';
import { DPlannerViewService } from './dayli-planner.view.service';
import { TModeType } from 'src/app/app-page/types/app.types';

@Injectable()
export class DPNotesViewService extends ServiceManager<DPNotesFlags, DPNoteState> {

    notesCache$: Map<string, TPaginationRes<IPNote>> = new Map();
    

    notes$: BehaviorSubject<TPaginationRes<IPNote>> = new BehaviorSubject(null);
    modeNote$: BehaviorSubject<IPNote> = new BehaviorSubject(null);
    
    constructor(
        private dpService: DayliPlannerService,
        private dpViewService: DPlannerViewService,
        private toastr: ToastrService
    ) {
        super(new DPNotesFlags, new DPNoteState, DBDayliPlanner)
    }

    fetch(dateStr: string, page: IPage, action: TListAction = 'replace') {

        this.setFlag('isFetchNotes', true);

        const cacheNotes: TPaginationRes<IPNote> = action === 'replace' && this.getFromCache(dateStr);

        if(cacheNotes) {
            cacheNotes.rows.sort((a, b) => a.DateInt - b.DateInt);
            this.notes$.next(cacheNotes);
            
            return this.setFlag('isFetchNotes', false);
        }
        
        this.dpService.fetchNotes(dateStr, page)
        .pipe(takeUntil(this.unubscriber$))
        .subscribe((response: TPaginationRes<IPNote>) => {

            if(action === 'replace') {
                this.notes$.next(response);
                
            } else {
                const notes: TPaginationRes<IPNote> = this.getStreamValue('notes$');
                const { rows, totalCount } = response;

                notes.rows.push(...rows);
                notes.totalCount = totalCount;
                response = notes;

                this.notes$.next(notes);
            }

            this.setNotesCache(dateStr, response)

            this.setFlag('isFetchNotes', false);
        })
    }

    getFromCache(dateStr: string): TPaginationRes<IPNote> {
        return this.notesCache$.get(dateStr) || null;
    }

    setNotesCache(datesStr: string, notes: TPaginationRes<IPNote>): void {
        this.notesCache$.set(datesStr, notes);
    }

    create(note: IPNote): void {
        this.setFlag('isModeNote', true);

        this.dpService.createNote(note).subscribe((createNote: IPNote) => {

            this.mutate(createNote, 'create');
            this.dpViewService.mutateCache(note);
            this.setFlag('isModeNote', false);
            this.hideModeModal()
            this.toastr.success(`Заметка ${createNote.Title} создана.`, 'Успешно!');
            this.detected();

        });
    }

    edit(note: IPNote): void {
        this.setFlag('isModeNote', true);

        this.dpService.editNote(note).subscribe((editNote: IPNote) => {

            this.mutate(editNote, 'edit');
            this.setFlag('isModeNote', false);
            this.hideModeModal();
            this.toastr.success(`Заметка ${editNote.Title} отредактированна.`, 'Успешно!');
            this.detected();
        });
    }

    remove(note: IPNote): void {

        this.setFlag('isModeNote', true);

        const noteIdsList: string[]= this.getStateValue('removeIdsList');
        this.setStateValue('removeIdsList', noteIdsList.concat(note._id));
        this.detected();

        this.dpService.removeNoteById(note._id)
        .subscribe(({ message }: TMessageRes) => {
            const updateIdsList: string[]= this.getStateValue('removeIdsList').filter(id => id !== note._id);
  
            this.mutate(note, 'remove');
            this.dpViewService.mutateCache(note);
            this.setFlag('isModeNote', false);
            this.setStateValue('removeIdsList', updateIdsList);
            this.setFlag('isModeNote', false);
            this.hideModeModal();
            this.setStateValue('removeNoteCandidate', null);
            this.toastr.success(message, 'Успешно!');
            this.detected();
        });
    }

    mutate(note: IPNote, action: TModeType): void {
        const notes: TPaginationRes<IPNote> = this.getStreamValue('notes$');

        switch(action) {
            case 'create': {
                notes.rows.unshift(note);
                notes.totalCount++;

                break;
            }

            case 'edit': {
                const noteIdx: number = notes.rows.findIndex(n => n._id === note._id);

                notes.rows[noteIdx] = note;

                break;
            }

            case 'remove': {
                const noteIdx: number = notes.rows.findIndex(n => n._id === note._id);

                notes.rows.splice(noteIdx, 1);
                notes.totalCount--;

                break;
            }
        }

        this.notes$.next(notes);
        this.setNotesCache(note.DateString, notes);
    }

    hideModeModal(): void {
        this.modeNote$.next(null);
        this.setStateValue('noteModeType', null);
    }
}