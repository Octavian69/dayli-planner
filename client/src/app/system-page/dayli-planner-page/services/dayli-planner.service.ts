import { Injectable } from '@angular/core';
import { ServicesModule } from 'src/app/app-page/modules/services.module';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IPage } from 'src/app/app-page/interfaces/app/IPage';
import { TPaginationRes, TMessageRes } from 'src/app/app-page/types/app-api.types';
import { IPNote } from '../interfaces/IPNote';
import { Observable } from 'rxjs';
import { IPDateStats } from '../interfaces/IPDateStats';
import { TDateType } from '../types/dayli-planner.types';
import { TFromTo } from 'src/app/app-page/types/app.types';
import { DPStatsPeriod } from '../models/DPStatsPeriod';
import { INoteFilter } from '../../history-page/interfaces/INoteFilter';
import { TAnalyticsRes, TAnalyticsYearPeriod } from '../../analytics-page/types/analytics.types';

@Injectable({
    providedIn: ServicesModule
})
export class DayliPlannerService {

    constructor(
        private http: HttpClient
    ) {}

    fetchNotes(DateString: string, page: IPage): Observable<TPaginationRes<IPNote>> {
        return this.http.post<TPaginationRes<IPNote>>(`@/dayli-planner/fetch-notes`, { DateString, page })
    }

    fetchFilterNotes(page: IPage, filters: INoteFilter): Observable<TPaginationRes<IPNote>> {
        return this.http.post<TPaginationRes<IPNote>>('@/dayli-planner/filter-notes', { page, filters });
    }

    fetchDatesStats(stats: DPStatsPeriod): Observable<IPDateStats[]> {
        return this.http.post<IPDateStats[]>('@/dayli-planner/fetch-date-stats', stats);
    }

    createNote(candidate: IPNote): Observable<IPNote> {
        return this.http.post<IPNote>('@/dayli-planner/create-note', candidate);
    }

    getNoteById(id: string): Observable<IPNote> {
        return this.http.get<IPNote>(`@/dayli-planner/get-note/${id}`);
    }

    editNote(note: IPNote): Observable<IPNote> {
        return this.http.put<IPNote>(`@/dayli-planner/edit-note/${note._id}`, note);
    }

    removeNoteById(id: string): Observable<TMessageRes> {
        return this.http.delete<TMessageRes>(`@/dayli-planner/remove-note/${id}`);
    }
}