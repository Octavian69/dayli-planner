import { Injectable } from '@angular/core';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';
import { DPlannerFlags } from '../flags/DPlannerFlags';
import { DBDayliPlanner } from '../db/daily-planner.db';
import { DPlannerState } from '../states/dplanner.state';
import { BehaviorSubject, Subject, of, Observable } from 'rxjs';
import { TChangeDate, TDateType, TDateGetter, TDateSetter, TCalendar, TModeYearOptions, TUpdateStats, TCahceStats } from '../types/dayli-planner.types';
import { PlannerStatesManager } from '../managers/PlannerStatesManager';
import { AuthViewService } from 'src/app/auth-page/services/auth.view.service';
import { IUser } from 'src/app/app-page/interfaces/models/IUser';
import { copyDate } from 'src/app/app-page/handlers/app-shared.handlers';
import { takeUntil, distinctUntilChanged, debounceTime, pairwise,switchMap, map } from 'rxjs/operators';
import { TSide, TFromTo } from 'src/app/app-page/types/app.types';
import { IPlannerManager } from '../interfaces/IPlannerManager';
import { IPlannerOption } from '../interfaces/IPlannerOption';
import { DPStatsPeriod } from '../models/DPStatsPeriod';
import { DayliPlannerService } from './dayli-planner.service';
import { IPDateStats } from '../interfaces/IPDateStats';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { IPNote } from '../interfaces/IPNote';
import { StorageService } from 'src/app/app-page/services/storage.service';

@Injectable()
export class DPlannerViewService extends ServiceManager<DPlannerFlags, DPlannerState> {
    statsCache$: Map<TDateType, ISimple<TCahceStats>>;

    date$: BehaviorSubject<Date> = new BehaviorSubject(null);
    stats$: Subject<DPStatsPeriod> = new Subject();
    statesManager$: PlannerStatesManager;
    dateMethods$: Map<TDateType, [TDateGetter, TDateSetter]> = new Map(
        this.getDataByArrayKeys(['dateMethods'])
    )

    constructor(
        private authService: AuthViewService,
        private dpService: DayliPlannerService,
        private storage: StorageService
    ) {
        super(new DPlannerFlags, new DPlannerState, DBDayliPlanner);
    }

    init(date: Date): void {
        const user: IUser = this.authService.getStreamValue('user$');
        const minDate: Date = copyDate(user.Created);
        const dateTypes: TDateType[] = this.getDataByArrayKeys(['dateTypes']);
    
        this.date$.next(date);

        this.statesManager$ = new PlannerStatesManager(date, minDate, dateTypes);

        this.initCache(dateTypes);
        this.initStepSub();
        this.initStatsSub();
    }

    initCache(dateTypes: TDateType[]): void {
        if(!this.statsCache$) {
            this.statsCache$ = new Map();

            dateTypes.forEach(type => {
                this.statsCache$.set(type, {});
            })
        }
    }

    initStatsSub(): void {
        this.stats$
        .pipe(
            debounceTime(300),
            switchMap((stats: DPStatsPeriod) => {
                const cache = this.getStatsFromCache(stats);
                const req$: Observable<IPDateStats[]> = cache && !cache.isUpdate
                 ? of(cache.options)
                 : this.dpService.fetchDatesStats(stats)

                if(cache && !cache.isUpdate) console.log('from cache');
                 
                return req$.pipe(
                    map((options: IPDateStats[]) => ({ options, stats }))
                )
            }),
            takeUntil(this.unubscriber$)
        )
        .subscribe((response: TUpdateStats) => {
            this.setStatsCache(response);

            this.statesManager$.updateOptions(response);
            this.detected();
        })
    }

    initStepSub(): void {
        this.getStream('date$').pipe(
            distinctUntilChanged(),
            pairwise(),
            map(([prevDate, currentDate]: [Date, Date]) => {
                const isUpdate: boolean = this.statesManager$.isChangePeriod(prevDate);

                if(isUpdate) {
                    this.statesManager$.completeStatesOptions();
                    this.statesManager$.updateStatesValues();
                    this.setFlag('isSetDate', true);
                    this.getStats('date', currentDate);

                } else {
                    const dateState: IPlannerManager = this.statesManager$.getState('date');
                    dateState.setValue(currentDate);
                    dateState.refreshSelectedOptions();
                    this.detected();
                }
            }),
            debounceTime(300),
            takeUntil(this.unubscriber$)
        ).subscribe(() => {
            this.setFlag('isSetDate', false);
            
            this.detected();
        })
    }

    change(opt: IPlannerOption): void {
        const date: Date = this.statesManager$.change(opt);
        const currentState: IPlannerManager = this.statesManager$.getCurrentState();
        const type = currentState?.type || 'date';

        this.date$.next(date);

        if(opt.type !== 'date') this.getStatsByType(type);
        
    }

    step(event: TChangeDate) {
        const date: Date = this.statesManager$.step(event);
       
        this.date$.next(date);
    }

    setCurrentState(type: TCalendar) {
        this.statesManager$.completeOptions(type);
        this.statesManager$.setCurrentState(type);
        this.detected();

        this.getStatsByType(type);
    }

    getCacheKey(period: TFromTo<string | number>): string {
        return Object.values(period).join('-');
    }

    setStatsCache(settings: TUpdateStats): void {
        const { stats: { Type, period }, options } = settings;
        const key: string = this.getCacheKey(period);
        const cache = this.statsCache$.get(Type);

        
        cache[key] = { options, isUpdate: false };
    }

    mutateCache(note: IPNote) {
        const dateTypes: TDateType[] = this.getDataByArrayKeys(['dateTypes']);
        const [, month, year] = note.DateString.split('.');
        const caches = dateTypes.reduce((accum, type: TDateType) => {
            accum[type] = this.statsCache$.get(type);

            return accum;
        }, {} as Record<TDateType, ISimple<TCahceStats>>);

        const keys = {
            date: () => `${month}.${year}`,
            month: () => String(year),
            year: (cache: ISimple<TCahceStats>) => Object.keys(cache).find(key => {
                const [ from, to ] = key.split('-');

                return  +year >= +from && +year <= +to;
            })
        }

        Object.entries(caches).forEach(([key, value]: [TDateType, ISimple<TCahceStats>]) => {
            const cacheKey = keys[key](value);

            if(value[cacheKey]) {
                value[cacheKey].isUpdate = true;
            }
        })
    }

    getStatsFromCache(settings: DPStatsPeriod): TCahceStats {
        const { Type, period } = settings;
        const key: string = this.getCacheKey(period);
        const cache = this.statsCache$.get(Type);

        return cache[key];
    }

    getStatsByType(type: TDateType): void {
        const date: Date = this.getStreamValue('date$');

        if(type === 'year') {
            const fromOpt = this.statesManager$.getOptionsByType(type)[0] as IPlannerOption;
            date.setFullYear(fromOpt.value);
        };

        this.getStats(type, date);
    }

    getStats(type: TDateType, date: Date): void {
        const stats: DPStatsPeriod = new DPStatsPeriod(type, date);

        this.stats$.next(stats);
    }

    setFullDate(d: Date): void {
        const date: Date = this.statesManager$.setFullDate(d);

        this.date$.next(date);
    }

    openCalendar(type: TCalendar): void {
        this.setCurrentState(type);
    }

    closeCalendar(): void {
        this.statesManager$.resetCurrentState();
        this.getStatsByType('date');
    }

    getCurrentStateType(): TCalendar {
        const currentState: IPlannerManager = this.statesManager$.getCurrentState();

        return currentState?.type as TCalendar || null;
    }

    changePeriod(side: TSide): void {
        const type: TModeYearOptions = 'set';
        this.getCurrentState().completeOptions({ side, type });

        const fromOpt = this.statesManager$.getOptionsByType('year')[0] as IPlannerOption;
        const date: Date = new Date();
        date.setFullYear(fromOpt.value);

        this.getStats('year', date);
    }

    getCurrentState(): IPlannerManager {
        return this.statesManager$.getCurrentState();
    }

    getMinDate(): Date {
        return this.statesManager$.getMinDate();
    }

    getCurrentOptions(): IPlannerOption[] {
        return this.statesManager$.getCurrentState()?.getOptions();
    }

    getOptions(type: TDateType): IPlannerOption[] | IPlannerOption[][] {
        return this.statesManager$.getOptionsByType(type) as IPlannerOption[] | IPlannerOption[][];
    }

    destoy(): void {
        this.reset(new DPlannerFlags, new DPlannerState);
    }
}