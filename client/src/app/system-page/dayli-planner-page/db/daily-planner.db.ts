export const DBDayliPlanner = {
    navList: [
        { id: 1, title: 'Календарь', icon: 'calendar_today', link: '/dayli-planner'},
        { id: 2, title: 'Заметки', icon: 'event_note', link: '/dayli-planner/notes'},
    ],
    dateTitles: {
        'year': 'Год',
        'month': 'Месяц',
        'date': 'День'
    },
    changeDateList: [
        { id: 'year', title: 'Год', state: { pattern: 'yyyy' }},
        { id: 'month', title: 'Месяц', state: { pattern: 'LLLL' }},
        { id: 'date', title: 'Число', state: { pattern: 'dd' }},
       
    ],
    priorityOptions: [
        { id: 1, title: 'Обычный', state: { pattern: 'yyyy' }},
        { id: 2, title: 'Важно', state: { pattern: 'LLLL' }},
        { id: 3, title: 'Необходимо', state: { pattern: 'dd' }},
    ],
    priorityTitles: {
        1: 'Обычный',
        2: 'Важно',
        3: 'Необходимо',
    },
    priorityColors: {
        1: 'dodgerblue',
        2: 'gold',
        3: 'tomato',
    },
    dateMethods: [
        ['date', ['getDate', 'setDate']],
        ['month', ['getMonth', 'setMonth']],
        ['year', ['getFullYear', 'setFullYear']],
        ['hours', ['getHours', 'setHours']],
        ['minutes', ['getMinutes', 'setMinutes']],
    ],
    months: [
        'Январь', 'Февраль', 'Март', 'Апрель',
        'Май', 'Июнь', 'Июль', 'Август',
        'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
    ],
    weekdays: ['Пн', 'Вт','Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    dateTypes: ['date', 'month', 'year'],
    fullDateTypes: ['year', 'month', 'date', 'hours', 'minutes']
} as const;