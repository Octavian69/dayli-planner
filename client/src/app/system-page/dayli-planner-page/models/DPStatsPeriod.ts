import { TDateType } from '../types/dayli-planner.types';
import { TFromTo } from 'src/app/app-page/types/app.types';
import { getDatesPreiodByType } from '../handlers/dayli-planner.handlers';

export class DPStatsPeriod {
    period: TFromTo<string | number>;

    constructor(
        public Type: TDateType,
        date: Date
    ) {
       this.period = getDatesPreiodByType(Type, date)
    }

}