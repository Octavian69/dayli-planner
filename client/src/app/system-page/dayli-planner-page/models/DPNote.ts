import { copyDate } from 'src/app/app-page/handlers/app-shared.handlers';
import { IPNote } from '../interfaces/IPNote';

export class DPNote implements IPNote {

    public Title: string = null;
    public Description: string = null;
    public Priority: number = 1;
    public Day: number = null;
    public Month: number = null;
    public Year: number = null;
    public DateString: string = null; 
    public DateInt: number = null;
    public User: string = null;

    constructor(date: Date) {
        this.set(copyDate(date));
    }

    set(date: Date) {
        const now: Date = new Date();

        date.setHours(now.getHours() + 1);
        date.setMinutes(now.getMinutes());

        this.Day = date.getDate();
        this.Month = date.getMonth();
        this.Year = date.getFullYear();
        this.DateString = date.toLocaleDateString();
        this.DateInt = date.getTime();
    }
}

// Title: string;
// Description: string;
// Priority: string;
// Date: number;
// Month: number;
// Year: number;
// DateString: string;
// DateInt: number;
// User: string;
// _id?: string;