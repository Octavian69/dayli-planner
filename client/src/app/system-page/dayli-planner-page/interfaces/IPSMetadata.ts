import { TDateType } from '../types/dayli-planner.types';
import { IPlannerManager } from './IPlannerManager';

export interface IPSMetadata {
    currentState: IPlannerManager;
    date: Date;
    states: Record<TDateType, IPlannerManager>;
}