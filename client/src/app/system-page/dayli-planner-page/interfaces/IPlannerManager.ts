import { IPlannerMetadata } from './IPMetadata';
import { TChangeDate, TUpdateStats } from '../types/dayli-planner.types';
import { IPlannerOption } from './IPlannerOption';
import { IPDateStats } from './IPDateStats';

export interface IPlannerManager extends IPlannerMetadata {
    init(): void;
    setValue(value: Date): void;
    change(opt: IPlannerOption): Date;
    step(event: TChangeDate): Date;
    getValue(): number;
    getOptions(): any[];
    updateOptions(data: TUpdateStats): void; 
    completeOptions(settings?: any): void;
    refreshSelectedOptions?(): void;
}