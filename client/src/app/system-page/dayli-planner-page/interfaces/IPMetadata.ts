
import { IPlannerStatesManager } from './IPlannerStatesManager';
import { TDateType } from '../types/dayli-planner.types';

export interface IPlannerMetadata {
    stateManager: IPlannerStatesManager;
    options: any[];
    value: number;
    type: TDateType;
}