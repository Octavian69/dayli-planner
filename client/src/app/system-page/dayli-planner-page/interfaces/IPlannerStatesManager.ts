import { IPSMetadata } from './IPSMetadata';
import { TChangeDate, TDateType, TUpdateStats } from '../types/dayli-planner.types';
import { IPlannerManager } from './IPlannerManager';

export interface IPlannerStatesManager extends IPSMetadata {
    init(states: TDateType[]): void;
    getMinDate(): Date; 
    setMinDate(date: Date): void;
    setFullDate(date: Date): Date
    step(event: TChangeDate): Date;
    change(event: TChangeDate): Date;
    getDate(): Date;
    getState(key: TDateType): IPlannerManager;
    getCurrentState(): IPlannerManager;
    getOptionsByType(type: TDateType): any[];
    setCurrentState(key: TDateType): void;
    updateStatesValues(): void;
    completeStatesOptions(): void;
    updateOptions(response: TUpdateStats): void;
    isChangePeriod(prevDate: Date): boolean;
}
