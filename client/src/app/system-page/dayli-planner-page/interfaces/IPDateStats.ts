import { TDateType } from '../types/dayli-planner.types';

export interface IPDateStats {
    Type: TDateType;
    NotesCount: number;
    DateString: string;
    DateInt: number;
    Value: number;
    User: string;
    _id?: string;
}