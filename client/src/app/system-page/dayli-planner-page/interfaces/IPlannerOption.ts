import { TDateType } from '../types/dayli-planner.types';
import { IPDateStats } from './IPDateStats';

export interface IPlannerOption {
    type: TDateType
    value: number;
    disabled: boolean;
    current: boolean;
    selected: boolean;
    dateString: string;
    stats: IPDateStats;
    title?: string;
}