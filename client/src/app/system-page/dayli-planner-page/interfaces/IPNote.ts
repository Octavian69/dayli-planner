export interface IPNote {
    Title: string;
    Description: string;
    Priority: number;
    Day: number;
    Month: number;
    Year: number;
    DateString: string;
    DateInt: number;
    User: string;
    _id?: string;
}