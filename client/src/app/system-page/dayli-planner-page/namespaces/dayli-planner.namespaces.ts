import { ILength } from 'src/app/app-page/interfaces/app/ILength';
import { IPNote } from '../interfaces/IPNote';
import { TNoteFormLengthKeys } from '../types/dayli-planner.types';

export namespace NSPlannerLimits {
    export const NOTES: number = 15;
    export const STATS_YEARS: number = 12;
}

export namespace DPFormLengths  {

    export const Note: Readonly<ILength> = {
        min: {
            Title: 8,
        },
        max: {
            Title: 25,
            Description: 150
        }
    }

}