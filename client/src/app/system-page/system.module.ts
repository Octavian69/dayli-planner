import { NgModule } from '@angular/core';
import { CoreModule } from '../app-page/modules/core.module';
import { SystemRoutingModule } from './system.routing.module';

import { SystemGuard } from './system-page/guards/system.guard';

import { SystemPageComponent } from './system-page/components/system-page/system-page.component';
import { SystemPageTitleComponent } from './system-page/components/system-page-title/system-page-title.component';
import { SystemViewService } from './system-page/services/system.view.service';
import { ScrollArrowComponent } from './system-page/components/scroll-arrow/scroll-arrow.component';
import { CircleNavComponent } from './system-page/components/circle-nav/circle-nav.component';
import { MaterialModule } from '../app-page/modules/material.module';

@NgModule({
    declarations: [
        SystemPageComponent,
        SystemPageTitleComponent,
        ScrollArrowComponent,
        CircleNavComponent
    ],
    imports: [
        CoreModule,
        MaterialModule,
        SystemRoutingModule
    ],
    providers: [
        SystemGuard,
        SystemViewService
    ]
})
export class SystemModule {}