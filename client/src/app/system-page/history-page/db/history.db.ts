export const DBHistory = {
    historyNoteTitles: {
        Title: 'Название',
        Description: 'Описание',
        DateInt: 'Дата',
        Priority: 'Приоритет',
        Status: 'Статус'
    },
    options: {
        status: [
            {title: 'Активные', id: 1},
            {title: 'Архивные', id: -1},
        ]
    },
    styles: {
        status: {
            '-1': {
                color: 'floralwhite',
                'text-shadow': '0 0 5px black, 0 0 10px black, 0 0 15px black, 0 0 20px black, 0 0 0px black, 0 0 7px black, 0 0 0px black, 0 0 0px black'
            },
            1: {
                color: 'dodgerblue'
            }

        }
    },
    historyNoteIds: {
        Title: 1,
        DateInt: 2,
        Priority: 3,
        Status: 4,
        Description: 5
    },
    historyNoteTypes: {
        DateInt: 'date',
    },
    factoryMethods: {
        Title: 'compelteDefault',
        Description: 'compelteDefault',
        DateInt: 'compelteDateInt',
        Priority: 'completePriority',
    }
}