import { INoteFilter } from '../interfaces/INoteFilter';

export class NoteFilter implements INoteFilter {

    public SortField: 'Priority' | 'DateInt' = 'DateInt';
    public Direction: 1 | -1 = 1;
    public Title: string = '';
    public Description: string = ''; 
    public Priority: number = null;
    public Status: number = null;

    constructor() {}
}