import { IHistoryNoteOption } from '../interfaces/IHistoryNoteOption';
import { THistoryOptionType, THistoryNoteState } from '../types/history.types';

export class HistoryNoteOption implements IHistoryNoteOption {
    constructor(
        public title: string,
        public value: string | number,
        public key: string,
        public id: number,
        public type: THistoryOptionType = 'default',
        public state?: THistoryNoteState
    ) {}
}