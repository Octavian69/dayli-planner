import { getDataByArrayKeys, compareDates } from 'src/app/app-page/handlers/app-shared.handlers';
import { DBDayliPlanner } from '../../dayli-planner-page/db/daily-planner.db';
import { DBHistory } from '../db/history.db';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { IPNote } from '../../dayli-planner-page/interfaces/IPNote';
import { HistoryNoteOption } from './HistoryNoteOption';
import { IHistoryNoteOption } from '../interfaces/IHistoryNoteOption';
import { THistoryOptionType, THistoryNoteState } from '../types/history.types';

const priorityTitles: ISimple<string> = getDataByArrayKeys(['priorityTitles'], DBDayliPlanner);
const priorityColors: ISimple<string> = getDataByArrayKeys(['priorityColors'], DBDayliPlanner);
const factoryMethods: ISimple<string> = getDataByArrayKeys(['factoryMethods'], DBHistory);
const historyNoteTitles: ISimple<string> = getDataByArrayKeys(['historyNoteTitles'], DBHistory);
const historyNoteIds: ISimple<number> = getDataByArrayKeys(['historyNoteIds'], DBHistory);
const historyNoteTypes: ISimple<THistoryOptionType> = getDataByArrayKeys(['historyNoteTypes'], DBHistory);


export class HistoryNoteFactory {

    constructor() {}

    public complete(note: IPNote): IHistoryNoteOption[] {
        const noteOptions: IHistoryNoteOption[] = Object.entries(note).reduce((accum, [key, value]) => {
            const method: string = factoryMethods[key];

            if(method) {
                const option: IHistoryNoteOption | IHistoryNoteOption[] = this[method](key, note);
                accum = accum.concat(option)
            } 

            return accum;
        }, [])



        return noteOptions.sort((a, b) => a.id - b.id);
    }

    private compelteDefault(key: string, note: IPNote): IHistoryNoteOption {
        const title: string = historyNoteTitles[key];
        const value: string = note[key] || `${title} отсутствует`;
        const id: number = historyNoteIds[key];

        return new HistoryNoteOption(title, value, key, id)
    }

    private compelteDateInt(key: string, note: IPNote): IHistoryNoteOption[] {
        const title: string = historyNoteTitles[key];
        const value: number = note[key];
        const id: number = historyNoteIds[key];
        const dateOpt: IHistoryNoteOption = new HistoryNoteOption(title, value, key, id, 'date');
        const statusOpt: IHistoryNoteOption = this.completeStatus(key, note)
            
        return [dateOpt, statusOpt];
    }

    private completePriority(key: string, note: IPNote): IHistoryNoteOption {
        const noteValue: number = note[key];
        const title: string = historyNoteTitles[key];
        const value: string = priorityTitles[noteValue];
        const id: number = historyNoteIds[key];
        const state: THistoryNoteState = { styles: { color: priorityColors[noteValue] } }

        return new HistoryNoteOption(title, value, key, id, 'default', state);
    }

    private completeStatus(key: string, note: IPNote): IHistoryNoteOption {
        const date: Date = new Date(note[key]);
        const isLess: boolean = compareDates(date, new Date, 'date', 'before');
        const title: string = 'Статус';
        const value: string = isLess ? 'Архив' : 'Активна';
        const id: number = historyNoteIds['Status'];
        const className: string = isLess ? 'less' : 'active';
        const state: THistoryNoteState = { classes: { [className]: true } } 

        return new HistoryNoteOption(title, value, 'Status', id, 'default', state);
    }
}