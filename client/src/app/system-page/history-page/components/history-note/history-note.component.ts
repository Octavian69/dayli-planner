import { Component, Input, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { IHistoryNoteOption } from '../../interfaces/IHistoryNoteOption';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { suffix } from 'src/app/app-page/handlers/app-shared.handlers';

@Component({
  selector: 'dp-history-note',
  templateUrl: './history-note.component.html',
  styleUrls: ['./history-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryNoteComponent implements OnInit {
  @Input('note') options:  [];

  ngOnInit() {
    // console.log(this.options);
  }

  getClass(className: string, opt: IHistoryNoteOption): ISimple<boolean> {
    const key: string = '_' + opt.key.toLocaleLowerCase();
    const siffixClass: string = suffix(className, key);

    const classObject: ISimple<boolean> = Object.assign(
      {[siffixClass]: true}, 
      opt?.state?.classes || {}
    )

    return classObject;
  }
}
