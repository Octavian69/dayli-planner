import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ANHeight } from 'src/app/app-page/animations/animations';
import { TShowStatus } from 'src/app/app-page/types/app.types';
import { INoteFilter } from '../../interfaces/INoteFilter';
import { deepCompare } from 'src/app/app-page/handlers/app-shared.handlers';
import { NoteFilter } from '../../models/NoteFilter';
import { IOption } from 'src/app/app-page/interfaces/app/IOption';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { Bind } from 'src/app/app-page/decorators/app-decorators';


@Component({
  selector: 'dp-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.scss'],
  animations: [ANHeight(500)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryFilterComponent implements OnInit, OnDestroy {

  form: FormGroup;
  filter: INoteFilter

  @Input('filter') set _filter(value: INoteFilter) {
    this.filter = value;

    if(this.form && value) this.form.patchValue(value)
  };
  @Input() show: boolean = true;
  @Input() loading: boolean = false;
  @Input() priorityOptions: IOption[];
  @Input() priorityColors: ISimple<string>;
  @Input() statusOptions: IOption[];
  @Input() statusColors: ISimple<any>;
  @Output('showFilter') _showFilter = new EventEmitter<boolean>();
  @Output('submit') _submit = new EventEmitter<INoteFilter>();

  constructor() { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy() {}

  init(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = new FormGroup({
      SortField: new FormControl('DateInt'),
      Direction: new FormControl(1),
      Title: new FormControl(''),
      Description: new FormControl(''),
      Priority: new FormControl(null),
      Status: new FormControl(null),
    }, { updateOn: 'change' });
  }

  formTouched(): void {
    this.form.markAsTouched();
  }

  getFilterTooltip() {
    const action: string = this.show ? 'Скрыть' : 'Показать'

    return `${action} панель фильтрации`;
  }

  @Bind()
  getPriorytyStyles(opt: IOption): ISimple<string> {
    const color: string = this.priorityColors[opt.id];

    return { color }
  }

  @Bind()
  getStatusStyles(opt: IOption): ISimple<string> {
    return  {
      paddingLeft: '1rem',
      ...this.statusColors[opt.id]
    }
  }

  getShowState(): TShowStatus {
    return this.show ? 'open' : 'close';
  }
 
  setShowState(): void {
    this._showFilter.emit(!this.show);
  }

  isActive(controlName: string, value: any): boolean {
    return this.form.get(controlName).value === value;
  }

  disabledForm(): boolean {
    const { untouched, dirty } = this.form;

    return untouched || !dirty || this.loading;
  }

  disabledAction(type: 'reset' | 'submit'): boolean {
    const { value } = this.form;

    switch(type) {
      case 'reset': {
        return deepCompare(value, new NoteFilter);
      }

      case 'submit': {
        return this.disabledForm() || deepCompare(value, this.filter); 
      }
    }
  }
  
  reset(): void {
    this.form.patchValue(new NoteFilter);
    this.submit();
  }

  submit(): void {
    const { value } = this.form;

    const values: INoteFilter = Object.entries(value).reduce((accum: INoteFilter, [key, value]) => {

      if(value) accum[key] = value;

      return accum;
    }, {} as INoteFilter)

    this._submit.emit(values);
    this.form.markAsUntouched();
  }
}
