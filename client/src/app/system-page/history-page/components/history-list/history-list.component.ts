import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { TPaginationRes } from 'src/app/app-page/types/app-api.types';
import { IPNote } from 'src/app/system-page/dayli-planner-page/interfaces/IPNote';
import { HistoryNoteFactory } from '../../models/HistoryNoteFactory';
import { IHistoryNoteOption } from '../../interfaces/IHistoryNoteOption';
import { ANParent, ANShowScale } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss'],
  animations: [ANParent(100), ANShowScale(300)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryListComponent {

  noteOptionFactory: HistoryNoteFactory = new HistoryNoteFactory()

  @Input() notes: TPaginationRes<IPNote>;
  @Input() loading: boolean = false;
  @Input() expand: boolean = false;

  trackByFn(idx: number, note: IPNote): string {
    return note._id;
  }

  completeNote(note: IPNote): IHistoryNoteOption[] {
    return this.noteOptionFactory.complete(note);
  }
}
