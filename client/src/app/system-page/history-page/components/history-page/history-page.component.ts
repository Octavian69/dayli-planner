import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoryViewService } from '../../services/history.view.service';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { HistoryFlags } from '../../metadata/HistoryFlags';
import { HistoryState } from '../../metadata/HistoryState';
import { TPaginationRes } from 'src/app/app-page/types/app-api.types';
import { IPNote } from 'src/app/system-page/dayli-planner-page/interfaces/IPNote';
import { Observable } from 'rxjs';
import { INoteFilter } from '../../interfaces/INoteFilter';
import { IPage } from 'src/app/app-page/interfaces/app/IPage';
import { Page } from 'src/app/app-page/classes/Page';
import { isCanExpand } from 'src/app/app-page/handlers/app-api.handlers';
import { StorageService } from 'src/app/app-page/services/storage.service';
import { NoteFilter } from '../../models/NoteFilter';
import { HistoryStorage } from '../../namespaces/history.namespaces';
import { NSPlannerLimits } from 'src/app/system-page/dayli-planner-page/namespaces/dayli-planner.namespaces';
import { IOption } from 'src/app/app-page/interfaces/app/IOption';
import { getDataByArrayKeys } from 'src/app/app-page/handlers/app-shared.handlers';
import { DBDayliPlanner } from 'src/app/system-page/dayli-planner-page/db/daily-planner.db';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';

@Component({
  selector: 'dp-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.scss']
})
export class HistoryPageComponent implements OnInit, OnInitMetatdata, OnDestroy {

  notes$: Observable<TPaginationRes<IPNote>> = this.viewService.getStream('notes$');

  flags: HistoryFlags;
  state: HistoryState;
  priorityOptions: IOption[] = getDataByArrayKeys(['priorityOptions'], DBDayliPlanner);
  priorityColors: IOption[] = getDataByArrayKeys(['priorityColors'], DBDayliPlanner);
  statusOptions: IOption[] = this.viewService.getDataByArrayKeys(['options', 'status']);
  statusColors: ISimple<any> = this.viewService.getDataByArrayKeys(['styles', 'status']);

  page: IPage = new Page(NSPlannerLimits.NOTES);

  constructor(
    private viewService: HistoryViewService,
    private storage: StorageService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.viewService.destroy();
  }

  ngOnInitMetatdata(): void {
    this.flags = this.viewService.getFlags();
    this.state = this.viewService.getState();
  }

  init() {
    this.ngOnInitMetatdata();
    this.initNotes();
  }

  initNotes(): void {
    const filter: INoteFilter = this.storage.getItem(HistoryStorage.FILTER) || new NoteFilter;
    this.viewService.fetch(this.page, 'replace', filter);
  }

  showFilter(flag: boolean): void {
    this.viewService.setFlag('isShowFilter', flag);
  }

  filter(filter: INoteFilter): void {
    this.page.skip = 0;
    this.viewService.fetch(this.page, 'replace', filter);
  }

  expand(): void {
    const notes: TPaginationRes<IPNote> = this.viewService.getStreamValue('notes$');
    const isExpand: boolean = isCanExpand(notes.rows, notes.totalCount);

    if(isExpand) {
      const filter: INoteFilter = this.viewService.getStateValue('filter');
      this.page.skip++;
      this.viewService.fetch(this.page, 'expand', filter);
    }
  }

}
