import { Injectable } from '@angular/core';
import { DayliPlannerService } from '../../dayli-planner-page/services/dayli-planner.service';
import { BehaviorSubject } from 'rxjs';
import { TPaginationRes, TListAction } from 'src/app/app-page/types/app-api.types';
import { IPNote } from '../../dayli-planner-page/interfaces/IPNote';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';
import { HistoryFlags } from '../metadata/HistoryFlags';
import { HistoryState } from '../metadata/HistoryState';
import { IPage } from 'src/app/app-page/interfaces/app/IPage';
import { INoteFilter } from '../interfaces/INoteFilter';
import { takeUntil } from 'rxjs/operators';
import { DBHistory } from '../db/history.db';
import { StorageService } from 'src/app/app-page/services/storage.service';
import { HistoryStorage } from '../namespaces/history.namespaces';

@Injectable()
export class HistoryViewService extends ServiceManager<HistoryFlags, HistoryState> {

    notes$: BehaviorSubject<TPaginationRes<IPNote>> = new BehaviorSubject(null);

    constructor(
        private dpService: DayliPlannerService,
        private storage: StorageService
    ) {
        super(new HistoryFlags, new HistoryState, DBHistory);
    }

    fetch(page: IPage, action: TListAction, filter: INoteFilter = null): void {
        const flag = action === 'replace' ? 'isFetch' : 'isExpand';

        this.setFlag(flag, true);
        

        this.dpService.fetchFilterNotes(page, filter).pipe(
            takeUntil(this.unubscriber$)
        ).subscribe((response: TPaginationRes<IPNote>) => {

            if(action === 'expand') {
                const notes: TPaginationRes<IPNote> = this.getStreamValue('notes$');
                response.rows = notes.rows.concat(response.rows);
            } 

            this.notes$.next(response);

            this.setStateValue('filter', filter);
            this.storage.setItem(HistoryStorage.FILTER, filter);
            this.setFlag(flag, false);
        })
    }

    destroy(): void {
        this.reset(new HistoryFlags, new HistoryState);
        this.storage.removeItem(HistoryStorage.FILTER);
    }
}