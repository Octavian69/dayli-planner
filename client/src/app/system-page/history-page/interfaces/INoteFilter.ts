
export interface INoteFilter {
    SortField: 'Priority' | 'DateInt';
    Direction: 1 | -1;
    Title: string;
    Description: string;
    Priority: number;
    Status: number;
}