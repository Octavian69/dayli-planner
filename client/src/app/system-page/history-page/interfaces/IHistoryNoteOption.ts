import { THistoryOptionType, THistoryNoteState } from '../types/history.types';

export interface IHistoryNoteOption {
    title: string;
    value: string | number;
    id: number;
    key: string;
    type: THistoryOptionType;
    state?: THistoryNoteState;
}