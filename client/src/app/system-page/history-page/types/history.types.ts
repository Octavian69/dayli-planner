import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';

export type THistoryOptionType = 'default' | 'date';

export type THistoryNoteState = {
    [key: string]: any;
    styles?: ISimple<string>;
    classes?: ISimple<boolean>;
}