import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/app-page/modules/core.module';
import { ComponentsModule } from 'src/app/app-page/modules/components.module';
import { MaterialModule } from 'src/app/app-page/modules/material.module';
import { HistoryRoutingModule } from './history.routing.module';
import { HistoryPageComponent } from './components/history-page/history-page.component';
import { HistoryFilterComponent } from './components/history-filter/history-filter.component';
import { HistoryListComponent } from './components/history-list/history-list.component';
import { HistoryViewService } from './services/history.view.service';
import { DirectivesModule } from 'src/app/app-page/modules/directives.module';
import { HistoryNoteComponent } from './components/history-note/history-note.component';

@NgModule({
    declarations: [
        HistoryPageComponent, 
        HistoryFilterComponent, 
        HistoryListComponent, HistoryNoteComponent
    ],
    imports: [
        CoreModule,
        ComponentsModule,
        MaterialModule,
        DirectivesModule,
        HistoryRoutingModule
    ],
    providers: [
        HistoryViewService
    ]
})
export class HistoryModule {}