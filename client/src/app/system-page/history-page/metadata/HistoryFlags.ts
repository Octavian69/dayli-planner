export class HistoryFlags {
    public isFetch: boolean = false;
    public isExpand: boolean = false;
    public isShowFilter: boolean = false;

    constructor() {}
}