import { INoteFilter } from '../interfaces/INoteFilter';
import { NoteFilter } from '../models/NoteFilter';

export class HistoryState {
    public filter: INoteFilter = new NoteFilter;
    
    constructor() {}
}