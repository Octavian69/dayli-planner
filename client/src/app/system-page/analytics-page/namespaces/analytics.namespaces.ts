export namespace NSAnalytics {
    export const YEAR_PERIOD: number = 5;
    export const YEAR_STORAGE_KEY: string = 'analytics-year-period';
}