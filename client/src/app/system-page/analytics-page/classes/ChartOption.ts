import { IPDateStats } from '../../dayli-planner-page/interfaces/IPDateStats';
import { TDateType } from '../../dayli-planner-page/types/dayli-planner.types';
import { IChartOption } from '../interfaces/IMonthChartOption';

export class ChartOption implements IChartOption {
    public name: any;
    public value: any;

    constructor(type: TDateType,stats: IPDateStats) {
        this.init(type, stats);
    }

    init(type: TDateType, stats: IPDateStats) {
        const { NotesCount } = stats;

        this.value = NotesCount;

        switch(type) {
            case 'year': {
                this.name = stats.Value;
                break;
            }
            case 'month': {
                this.name = new Date(stats.DateInt) 
                break;
            }
        }
    }
}