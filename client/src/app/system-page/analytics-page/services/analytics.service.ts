import { Injectable } from '@angular/core';
import { ServicesModule } from 'src/app/app-page/modules/services.module';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TAnalyticsRes, TAnalyticsYearPeriod } from '../types/analytics.types';
import { TDateType } from '../../dayli-planner-page/types/dayli-planner.types';
import { IPDateStats } from '../../dayli-planner-page/interfaces/IPDateStats';

@Injectable({
    providedIn: ServicesModule
})
export class AnalyticsService {

    constructor(private http: HttpClient) {}

    fetchForPeriod(type: Exclude<TDateType, 'date'> = 'year', period: number = 0) {
        const params: HttpParams = new HttpParams({fromObject: { period: String(period), type }});

        return this.http.get<IPDateStats[]>('@/analytics/fetch-by-period', { params });
    }
}