import { Injectable } from '@angular/core';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';
import { BehaviorSubject, forkJoin, Observable, of, throwError } from 'rxjs';
import { TAnalyticsYearPeriod } from '../types/analytics.types';
import { takeUntil, map, delay, retry, catchError, retryWhen, take, mergeMapTo, concatMapTo, tap, skip } from 'rxjs/operators';
import { DBAnalytics } from '../db/analytics.db';
import { AnalyticsService } from './analytics.service';
import { NSAnalytics } from '../namespaces/analytics.namespaces';
import { StorageService } from 'src/app/app-page/services/storage.service';
import { IUser } from 'src/app/app-page/interfaces/models/IUser';
import { IPDateStats } from '../../dayli-planner-page/interfaces/IPDateStats';
import { AnalyticsFlags } from '../metadata/AnalyticsFlags';
import { AnalyticsState } from '../metadata/AnalyticsState';
import { TDateType } from '../../dayli-planner-page/types/dayli-planner.types';
import { ChartOption } from '../classes/ChartOption';
import { IChartOption, IMonthChartOption } from '../interfaces/IMonthChartOption';

@Injectable()
export class AnalyticsViewService extends ServiceManager<AnalyticsFlags, AnalyticsState> {

    year$: BehaviorSubject<IChartOption[]> = new BehaviorSubject(null);
    month$: BehaviorSubject<[IMonthChartOption]> = new BehaviorSubject(null);

    constructor(
        private storage: StorageService,
        private analyticsService: AnalyticsService
    ) {
        super(new AnalyticsFlags, new AnalyticsState, DBAnalytics);
    }

    fetch(): void {
        const yearPeriodType: TAnalyticsYearPeriod = this.storage.getItem(NSAnalytics.YEAR_STORAGE_KEY) || 'period';
        const yearPeriod: number = this.getYearPeriod(yearPeriodType);

        const monthReq$ = this.analyticsService.fetchForPeriod('month');
        const yearReq$ = this.analyticsService.fetchForPeriod('year', yearPeriod);

        this.setStateValue('yearPeriodType', yearPeriodType);
        this.setFlag('isFetch', true);
        yearReq$.pipe(
            tap(res => this.completeStats('year', res)),
            concatMapTo(monthReq$),
            tap(res => this.completeStats('month', res)),
            takeUntil(this.unubscriber$),
        )
        .subscribe(_ => {
            this.setFlag('isFetch', false)
        });
    }

    setYearPeriod(periodType: TAnalyticsYearPeriod): void {
        const period: number = this.getYearPeriod(periodType);

        this.setFlag('isSetPeriod', true);

        this.analyticsService.fetchForPeriod('year', period)
            .pipe(
                takeUntil(this.unubscriber$)
            ).subscribe((res: IPDateStats[]) => {
                this.setFlag('isSetPeriod', false);
                this.completeStats('year', res);
                this.storage.setItem(NSAnalytics.YEAR_STORAGE_KEY, periodType);
                this.setStateValue('yearPeriodType', periodType);
            })
                
    }

    completeStats(type: TDateType, stats: IPDateStats[]): void {
        const options: IChartOption[] = stats.map(s => new ChartOption(type, s));

        switch(type) {
            case 'year': {
                this.year$.next(options);
                break;
            }

            case 'month': {
                const name: string = 'За месяц';
                this.month$.next([{ name, series: options }]);
                break;
            }
        }
    }

    getYearPeriod(periodType: TAnalyticsYearPeriod): number {

        let period: number;

        switch(periodType) {

            case 'all': {
                const { Created }: IUser = this.storage.getItem<IUser>('user');
                period = new Date().getFullYear() - new Date(Created).getFullYear();
                break;
            }

            case 'current': {
                period = 0;
                break;
            }

            case 'period': {
                period = NSAnalytics.YEAR_PERIOD;
                break;
            }
        }
        

        return period;
    }

    destroy() {
        this.reset(new AnalyticsFlags, new AnalyticsState);
        this.storage.removeItem(NSAnalytics.YEAR_STORAGE_KEY);
    }
}

