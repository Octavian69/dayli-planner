export interface IMonthChartOption {
    name: string;
    series: IChartOption[]
}

export interface IChartOption {
    value: any;
    name: any;
}