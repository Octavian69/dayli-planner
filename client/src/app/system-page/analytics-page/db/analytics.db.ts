export const DBAnalytics = {
    yearOptions: [
        { key: 'За текущий год', value: 'current' },
        { key: 'За последние 5 лет', value: 'period' },
        { key: 'Полный список с момента регистрации', value: 'all' },
    ],

    charts: {
        year: {
            colorScheme:{
              domain: [
                '#a8385d', 
                '#7aa3e5', 
                '#a27ea8', 
                '#aae3f5', 
                '#adcded',
                '#a95963',
              ]
            },
            legend: true,
            legendTitle: 'Список лет',
            showXAxis: true,
            showYAxis: true,
            gradient: false,
            showLegend: true,
            showXAxisLabel: true,
            xAxisLabel: 'Годы',
            showYAxisLabel: true,
            yAxisLabel: 'Заметки',
        },

        month: {
            legend: true,
            schemeType: 'linear',
            colorScheme:{
              domain: [
                'rgb(172, 216, 195)', 
                'rgb(225, 182, 117)', 
                'rgb(208, 153, 102)', 
                'rgb(155, 200, 193)', 
                'rgb(132, 167, 183)'
              ]
            },
            showLabels: true,
            animations: true,
            xAxis: true,
            yAxis: true,
            showYAxisLabel: true,
            showXAxisLabel: true,
            xAxisLabel: 'Даты',
            yAxisLabel: 'Заметки',
            timeline: true,
            legendPosition: 'below'
        }
    }
}