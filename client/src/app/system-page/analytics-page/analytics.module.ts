import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CoreModule } from 'src/app/app-page/modules/core.module';
import { AnalyticsPageComponent } from './components/analytics-page/analytics-page.component';
import { AnalyticsRoutingModule } from './analytics.routing.module';
import { AnalyticsChartsComponent } from './components/analytics-charts/analytics-charts.component';
import { AnalyticsViewService } from './services/analytics.view.service';
import { MaterialModule } from 'src/app/app-page/modules/material.module';
import { ComponentsModule } from 'src/app/app-page/modules/components.module';

@NgModule({
    declarations: [
        AnalyticsPageComponent,
        AnalyticsChartsComponent
    ],
    imports: [
        CoreModule,
        ComponentsModule,
        NgxChartsModule,
        MaterialModule,
        AnalyticsRoutingModule
    ],
    providers: [
        AnalyticsViewService
    ]
})
export class AnalyticsModule {}