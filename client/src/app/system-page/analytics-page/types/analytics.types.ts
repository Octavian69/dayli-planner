import { IPDateStats } from '../../dayli-planner-page/interfaces/IPDateStats';

export type TAnalyticsYearPeriod = 'current' | 'period' | 'all';

export type TAnalyticsRes = {
    yearStats: IPDateStats[];
    monthStats: IPDateStats[];
}
