import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';
import { TAnalyticsYearPeriod } from '../../types/analytics.types';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { FormControl } from '@angular/forms';
import { TControlStatus } from 'src/app/app-page/forms/types/app-form.types';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';
import { IMonthChartOption, IChartOption } from '../../interfaces/IMonthChartOption';
import { formatDate } from '@angular/common';
import { ANScaleState } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-analytics-charts',
  templateUrl: './analytics-charts.component.html',
  styleUrls: ['./analytics-charts.component.scss'],
  animations: [ANScaleState()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnalyticsChartsComponent implements OnInit {

  yearControl: FormControl;
  disabled: boolean = false;

  @Input('disabled') set _disabled(value: boolean) {

    if(this.yearControl) {
      const method: TControlStatus = value ? 'disable' : 'enable';

      this.yearControl[method]();
      this.disabled = value;
    }
  }
  @Input() monthSettings: ISimple<any>;
  @Input() yearSettings: ISimple<any>;
  @Input('yearData') yearData: IChartOption[];
  @Input('monthData') monthData: IMonthChartOption;
  @Input() yearPeriod: TAnalyticsYearPeriod;
  @Input() yearOptions: TKeyValue<TAnalyticsYearPeriod>[] = [];
  @Output('setYearPeriod') _setYearPeriod = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.init();
  }

  init() {
    console.log(this.getYearYAxisTicks());
    
    this.initYearControl();
  }
  
  initYearControl(): void {
    this.yearControl = new FormControl(this.yearPeriod);
  }

  getYearTitle(): string {
    const current: TKeyValue<TAnalyticsYearPeriod> = this.yearOptions.find(opt => opt.value === this.yearPeriod);

    return 'Статистика ' + current.key.toLocaleLowerCase();
  }

  getMonthDateTooltip(date: Date): string {
    return formatDate(date, 'dd MMMM (E)', 'ru');
  }

  getMonthSeriesTooltip(date: Date): string {
    return formatDate(date, 'dd MMMM (E)', 'ru');
  }

  getXAxisDateTicks(date: Date) {
    return formatDate(date, 'd MMM', 'ru');
  }

  getYearYAxisTicks(): number[] {
    const sorted: number[] = this.yearData
      .concat()
      .sort((a, b) => a.value - b.value)
      .map(opt => opt.value);

    const maxNotes: number = sorted[sorted.length - 1] + 1;

    let percentage: number;

    switch(true) {
      case maxNotes <= 10: {
        percentage = 5;
        break;
      }
      case maxNotes < 100: {
        percentage = 10;
        break;
      }
      case maxNotes >= 100: {
        percentage = 100;
        break;
      }
    }

    return Array.from({ length: maxNotes + 1 }, (v, k) => k).filter((idx: number) => {
      return sorted.includes(idx) || !(idx % percentage);
    });
  }

  getYAxisFormatting(value: number): string {
    const fixed: string = value.toFixed(0);

    return fixed !== String(value) ? '' : fixed;
  }

  getQuestionTooltip(): string {
    const date: Date = new Date();
    const to: string = date.toLocaleDateString();
    date.setMonth(date.getMonth() - 1);
    const from: string = date.toLocaleDateString();

    return `Статистика в промежуток c ${from} по ${to}`;
  }

  
  setYearPeriod(periodType: TAnalyticsYearPeriod): void {
    this._setYearPeriod.emit(periodType);
  }
}
