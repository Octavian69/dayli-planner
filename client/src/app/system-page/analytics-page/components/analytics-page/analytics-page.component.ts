import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnalyticsViewService } from '../../services/analytics.view.service';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { TAnalyticsYearPeriod } from '../../types/analytics.types';
import { OnInitMetatdata } from 'src/app/app-page/interfaces/hooks/OnInitMetadata';
import { AnalyticsFlags } from '../../metadata/AnalyticsFlags';
import { AnalyticsState } from '../../metadata/AnalyticsState';
import { Observable } from 'rxjs';
import { IChartOption, IMonthChartOption } from '../../interfaces/IMonthChartOption';
import { ISimple } from 'src/app/app-page/interfaces/app/ISimple';

@Component({
  selector: 'dp-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.scss']
})
export class AnalyticsPageComponent implements OnInit, OnInitMetatdata, OnDestroy {

  year$: Observable<IChartOption[]> = this.viewService.getStream<IChartOption[]>('year$');
  month$: Observable<IMonthChartOption[]> = this.viewService.getStream<IMonthChartOption[]>('month$');

  flags: AnalyticsFlags;
  state: AnalyticsState;
  yearSettings: ISimple<any> = this.viewService.getDataByArrayKeys(['charts', 'year']);
  monthSettings: ISimple<any> = this.viewService.getDataByArrayKeys(['charts', 'month']);
  yearOptions: TKeyValue<TAnalyticsYearPeriod>[] = this.viewService.getDataByArrayKeys(['yearOptions']);

  constructor(
    private viewService: AnalyticsViewService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnInitMetatdata(): void {
    this.flags = this.viewService.getFlags();
    this.state = this.viewService.getState();
  }

  ngOnDestroy() {
    this.viewService.destroy();
  }

  init(): void {
    this.ngOnInitMetatdata();
    this.viewService.fetch();
  }

  setYearPeriod(periodType: TAnalyticsYearPeriod): void {
    this.viewService.setYearPeriod(periodType)
  }

  disabledForm(): boolean {
    const { isFetch, isSetPeriod } = this.flags;

    return isFetch || isSetPeriod;
  }

}
