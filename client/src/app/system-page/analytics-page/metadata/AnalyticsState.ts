import { TAnalyticsYearPeriod } from '../types/analytics.types';

export class AnalyticsState {
    public yearPeriodType: TAnalyticsYearPeriod = 'period';

    constructor() {}
}