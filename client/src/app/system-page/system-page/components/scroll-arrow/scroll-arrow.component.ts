import { Component, HostListener, Inject, ChangeDetectionStrategy } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ANShowScale } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-scroll-arrow',
  templateUrl: './scroll-arrow.component.html',
  styleUrls: ['./scroll-arrow.component.scss'],
  animations: [ANShowScale()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScrollArrowComponent {

  public isShow: boolean = false;


  @HostListener('window:scroll', ['$event'])
    scroll(e: Event): void {
      const { scrollTop } = this.document.documentElement;

      this.isShow = scrollTop > 300;
    }

  constructor(
    @Inject(DOCUMENT) private document: Document,
  ) { }

  scrollToStartPage(): void {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }
}
