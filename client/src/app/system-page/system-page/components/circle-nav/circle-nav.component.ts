import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Router, UrlTree, Params } from '@angular/router';
import { ILinkOption } from 'src/app/app-page/interfaces/app/ILinkOption';
import { dateToQuery } from 'src/app/system-page/dayli-planner-page/handlers/dayli-planner.handlers';

@Component({
  selector: 'dp-circle-nav',
  templateUrl: './circle-nav.component.html',
  styleUrls: ['./circle-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CircleNavComponent {

  @Input() options: ILinkOption[];

  constructor(
    private router: Router
  ) { }

  trackByFn(idx: number, opt: ILinkOption): number {
    return opt.id as number;
  }

  navigate(link: string): void {
    const queryParams: Params = link === '/dayli-planner' ? { date: dateToQuery(new Date) } : {};

    this.router.navigate([link], { queryParams });
    
  }


}
