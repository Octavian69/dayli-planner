import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TKeyValue } from 'src/app/app-page/types/app.types';
import { SystemViewService } from '../../services/system.view.service';
import { ILinkOption } from 'src/app/app-page/interfaces/app/ILinkOption';

@Component({
  selector: 'dp-system-page',
  templateUrl: './system-page.component.html',
  styleUrls: ['./system-page.component.scss']
})
export class SystemPageComponent implements OnInit {

  headers: TKeyValue<string>[] = this.viewService.getDataByArrayKeys(['headers']);
  circleMenuItems: ILinkOption[] = this.viewService.getDataByArrayKeys(['circleMenuItems']);

  constructor(
    private router: Router,
    private viewService: SystemViewService
  ) { }

  ngOnInit(): void {}

  getTitle(): string {
    const option: TKeyValue<string> = this.headers
      .find(opt => this.router.isActive(opt.key, false));
    
    return option?.value;
  }
}
