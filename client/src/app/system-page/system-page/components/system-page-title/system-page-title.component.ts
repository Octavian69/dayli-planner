import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { ANTranslateState } from 'src/app/app-page/animations/animations';

@Component({
  selector: 'dp-system-page-title',
  templateUrl: './system-page-title.component.html',
  styleUrls: ['./system-page-title.component.scss'],
  animations: [ANTranslateState(400)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemPageTitleComponent implements OnInit {

  @Input() title: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
