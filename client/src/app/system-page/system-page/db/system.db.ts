export const DBSystem = {
    headers: [
        {key: '/dayli-planner', value: 'Ежедневник'},
        {key: '/history', value: 'История заметок'},
        {key: '/analytics', value: 'Аналитика'},
    ],
    circleMenuItems: [
        { 
            id: 1, 
            title: 'История заметок', 
            icon: 'history_edu', 
            link: '/history' ,
            state: {
                bg: 'tomato'
            }
        },
        { 
            id: 2, 
            title: 'Аналитика', 
            icon: 'assessment', 
            link: '/analytics' ,
            state: {
                bg: 'dodgerblue'
            }
        }
    ]
} as const;