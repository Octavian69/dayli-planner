import { Injectable } from '@angular/core';
import { ServiceManager } from 'src/app/app-page/managers/ServiceManager';
import { DBSystem } from '../db/system.db';

@Injectable()
export class SystemViewService extends ServiceManager {
    constructor() {
        super(null, null, DBSystem);
    }
}