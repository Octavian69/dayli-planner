import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthViewService } from 'src/app/auth-page/services/auth.view.service';
import { ToastrService } from 'ngx-toastr';
import { DBMessages } from 'src/app/app-page/db/app-msgs.db';
import { getDataByArrayKeys } from 'src/app/app-page/handlers/app-shared.handlers';

@Injectable()
export class SystemGuard implements CanActivate, CanActivateChild {
    constructor(
        private router: Router,
        private toastr: ToastrService,
        private authViewService: AuthViewService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    
    {
        const isLogged: boolean = this.authViewService.getLoggedStatus();

        if(!isLogged) {
            const MSGCredentials: string = getDataByArrayKeys(['credentials'], DBMessages);
            this.toastr.warning(MSGCredentials, 'Внимание.');
            this.router.navigate(['/login']);
            
            return false;
        }

        return true
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean
    {
        return this.canActivate(route, state);
    }
}