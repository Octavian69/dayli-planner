import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemPageComponent } from './system-page/components/system-page/system-page.component';
import { SystemGuard } from './system-page/guards/system.guard';
import { AnalyticsModule } from './analytics-page/analytics.module';

const routes: Routes = [
    { path: '', component: SystemPageComponent, canActivate: [SystemGuard], children: [
        { path: '', redirectTo: '/dayli-planner', pathMatch: 'full' },
        { 
            path: 'dayli-planner', 
            loadChildren: () => import('./dayli-planner-page/dayli-planner.module')
                .then(({DayliPlannerModule}) => DayliPlannerModule) },
        {
            path: 'history',
            loadChildren: () => import('./history-page/history.module')
                .then(({ HistoryModule }) => HistoryModule)
        },
        {
            path: 'analytics',
            loadChildren: () => import('./analytics-page/analytics.module')
                .then(({ AnalyticsModule }) => AnalyticsModule)
        }
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystemRoutingModule {}