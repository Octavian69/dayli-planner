import { IEnvironment } from 'src/app/app-page/interfaces/app/IEnvironment';

export const environment: IEnvironment = {
  production: true,
  API_URL: 'https://ng-dayli-planner.herokuapp.com'
};
