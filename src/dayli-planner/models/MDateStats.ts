import * as mongoose from 'mongoose';
import { IDateStats } from '../interfaces/IDateStats';
const { Schema, Types: { ObjectId }, model } = mongoose;

const DateStatsSchema = new Schema<IDateStats>({
    Type: {
        type: String,
        required: true
    },
    NotesCount: {
        type: Number,
        required: true
    },
    DateString: {
        type: String,
        required: true
    },
    DateInt: {
        type: Number,
        required: true
    },
    Value: {
        type: Number,
        required: true
    },
    User: {
        type: String,
        refs: 'user',
        required: true
    }
});

export const DateStatsModel = model<IDateStats>('date', DateStatsSchema)