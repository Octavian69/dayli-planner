import * as mongoose from 'mongoose';
import { IDateNote } from '../interfaces/IDateNote';
const { Schema, model, Types: { ObjectId } } = mongoose;

export const DateNoteSchema: mongoose.Schema<IDateNote> = new Schema({
    Title: {
        type: String,
        requred: true
    },
    Description: {
        type: String,
        default: null
    },
    Priority: {
        type: Number,
        default: 1
    },
    Year: {
        type: Number,
        required: true,
    },
    Month: {
        type: Number,
        required: true
    },
    Day: {
        type: Number,
        required: true
    },
    DateString: {
        type: String,
        required: true
    },
    DateInt: {
        type: Number,
        required: true
    },
    User: {
        type: String,
        refs: 'user',
        required: true
    }
})

export const DateNoteModel = model<IDateNote>('date-notes', DateNoteSchema);