import { noteStatusFilterHandler } from "../handlers/dayli-planner.handlers";

export const DBDayliPlanner = {
    dateTypes: ['date', 'month', 'year'],

    notes: {

        fieldTypes: {
            SortField: '$sort'
        },
        filterFuncs: {
            Title: ({ Title }) => ( {Title: {$regex: new RegExp(Title, 'gi')}} ),
            Description: ({ Description }) => ( {Description: {$regex: new RegExp(Description, 'gi')}} ),
            Status: ({ Status }) => noteStatusFilterHandler(Status),
            Priority: ({ Priority }) => ({ Priority: { $eq: Priority } })
        },
        sortFuncs: {
            SortField: ({ SortField, Direction }) => ( {[SortField]: Direction} ),
        }
    }


}