import { Router} from 'express';
import { 
    fetchNotes, fetchDatesStatsByType, 
    fetchFilterNotes, getDateNoteById, 
    createDayliNote, editDateNoteById, 
    removeDayliNoteById } from '../controllers/daily-planner.controller';
const router = Router();


router.post('/fetch-notes', fetchNotes);
router.post('/fetch-date-stats', fetchDatesStatsByType);
router.post('/filter-notes', fetchFilterNotes);
router.post('/create-note', createDayliNote);
router.get('/get-note/:id', getDateNoteById);
router.put('/edit-note/:id', editDateNoteById);
router.delete('/remove-note/:id', removeDayliNoteById);


export const dayliPlannerRouter: Router = router;