export class CDateStatsNote {
    public Title: string = null;
    public Description: string = null;
    public Priority: string = null;
    public Year: number = null;
    public Month: number = null;
    public Day: number = null;
    public DateInt: number = null;
    public DateString: string = null;
    public User: string;

    constructor() {}

}