import * as moment from 'moment';
import { TDate } from "../types/dayli-model.types";
import { getDateStringByType } from "../handlers/dayli-planner.handlers";

export class CDateStats {
    public DateString: string;
    public DateInt: number;
    public NotesCount: number = 1;
    public Value: number;

    constructor(
        date: number,
        public Type: TDate,
        public User: string
    ) {
        this.set(date);
    }

    set(d: number): void {
        const date: moment.Moment = moment(d);
        const dStr: string = date.format('DD.MM.YYYY');
        this.DateString = getDateStringByType(dStr, this.Type);
        this.Value = +moment(date).get(this.Type);
        this.DateInt = d;
    }
}