import { Document } from "mongoose";

export interface IDateNote extends Document {
    Title: string;
    Description: string;
    Priority: string;
    Day: number;
    Month: number;
    Year: number;
    DateString: string;
    DateInt: number;
    User: string;
}