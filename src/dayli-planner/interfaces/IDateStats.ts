import { Document } from "mongoose";
import { TDate } from "../types/dayli-model.types";

export interface IDateStats extends Document {
    Type: TDate;
    NotesCount: number;
    DateString: string;
    DateInt: number;
    Value: number;
    User: string;
}