import * as moment from 'moment';
import { TDate, TDateParameter, TSplitDate, TNoteFilter } from "../types/dayli-model.types"
import { TFromTo, Simple, TFieldFilterType } from "../../shared/types/shared.types";
import { getDataByArrayKeys } from '../../shared/handlers/app.handlers';
import { DBDayliPlanner } from '../db/dayli-planner.db';

export function getCopyDate(d: TDateParameter): Date {
    const date: Date = new Date(d);

    if(date instanceof Date) return date;

    throw new Error('Invalid Parameter Date format');
}

export function getDatePeriod(d: TDateParameter, type: TDate): TFromTo<number> {
    const date: Date = getCopyDate(d);
    const [CurrentDate, Month, Year] = date.toLocaleDateString()
        .split('.').map(d => +d);

    let from: number;
    let to: number;

    date.setHours(0, 0, 0, 0);

    switch(type) {
        case 'date': {
            from = date.getTime();
            date.setDate(CurrentDate + 1);
            date.setMilliseconds(-1);
            to = date.getTime();
            break;
        }
        case 'month': {
            date.setDate(1);
            from = date.getTime();
            date.setMonth(Month + 1);
            date.setMilliseconds(-1);
            to = date.getTime();
            break;
        }
        case 'year': {
            date.setMonth(0);
            date.setDate(1);
            from = date.getTime();
            date.setFullYear(Year + 1);
            date.setMilliseconds(-1);
            to = date.getTime();
            break;
        }
    }

    return { from, to };
}


export function getDateValueByType(d: TDateParameter, type: TDate): number {
    const date = moment(d);

    switch(type) {
        case 'date': return +date.get('date');
        case 'month': return +date.get('month');
        case 'year': return +date.get('year');
    }
}

export function getDateStringByType(date: string, type: TDate): string {
    const splitDate: TSplitDate = date.split('.') as TSplitDate
    const [ , Month, Year ] = splitDate;
    
    switch(type) {
        case 'date': return splitDate.join('.');
        case 'month': return `${Month}.${Year}`;
        case 'year': return String(Year);
    }
}

export function noteStatusFilterHandler(Status: number): Simple<Simple<number>> {
    const now: Date = new Date();
    const mongoParam: string =  Status === -1 ? '$lt' : '$gte';
    
    now.setHours(0, 0, 0 ,0);

    const expression = { DateInt: { [mongoParam]: +now } };

    return expression;
}

export function completNotesFilters(filters: TNoteFilter) {
    const fieldTypes: Simple<TFieldFilterType> = getDataByArrayKeys(['notes', 'fieldTypes'], DBDayliPlanner);
    const filterFuncs = getDataByArrayKeys(['notes', 'filterFuncs'], DBDayliPlanner, false);
    const sortFuncs = getDataByArrayKeys(['notes', 'sortFuncs'], DBDayliPlanner, false);
    
    

    const completeFilters = Object.entries(filters).reduce((accum, [key, value]) => {
        const fieldType = fieldTypes[key] || '$match';
        const funcObject = fieldType === '$sort' ? sortFuncs : filterFuncs;
        const fn = funcObject[key];

        if(fn && Boolean(value)) {
            Object.assign(accum[fieldType], fn(filters))
        }

        return accum;
    }, {$match: {}, $sort: {}});

    return completeFilters;
}