import { Response } from "express";
import { errorHandler } from "../../shared/handlers/shared-error.handlers";
import { IAccessRequest } from "../../shared/interfaces/app/IAccessRequest";
import { DateNoteModel } from "../models/MDateNote";
import { IDateNote } from "../interfaces/IDateNote";
import { IDateStats } from "../interfaces/IDateStats";
import { TDate, TDNoteCandidate, NotesFetchBody, DateStatsFetchBody, TFilterNotesBody } from "../types/dayli-model.types";
import { DateStatsModel } from "../models/MDateStats";
import { IMessage } from '../../shared/interfaces/app/IMessage';
import { CDateStats } from "../classes/CDateStats";
import { getDateStringByType, completNotesFilters } from "../handlers/dayli-planner.handlers";
import { getDataByArrayKeys } from "../../shared/handlers/app.handlers";
import { DBDayliPlanner } from "../db/dayli-planner.db";
import { TPaginationRes, Simple } from "../../shared/types/shared.types";
import { paginationAggregate } from "../../shared/handlers/app-mongo.handlers";


export async function fetchNotes(req: IAccessRequest<NotesFetchBody>, res: Response<TPaginationRes<IDateNote>>) {
    try {
        const { _id: User } = req.user;

        const { DateString, page: { skip, limit } } = req.body;
        const expressions = [
            { $match: { DateString, User } },
            { $sort: { DateInt: 1 } },
            ...paginationAggregate(skip, limit)
        ]

        const [result] = await DateNoteModel.aggregate(expressions);
        const rows: IDateNote[] = result?.rows || [];
        const totalCount: number = result?.totalCount || 0;

        
        res.status(200).json({ rows, totalCount });

    } catch(error) {
        errorHandler(res, error);
    }
}


export async function fetchDatesStatsByType(req: IAccessRequest<DateStatsFetchBody>, res: Response<IDateStats[]>) {
    try {
        const { Type, period } = req.body; 
        const { _id: User } = req.user;
        const expressions: any = { User, Type, NotesCount: { $gt: 0 } };        

        switch(Type) {
            case 'year': {
                expressions.$and = [ { Value: { $gte: period.from }  }, { Value: { $lte: period.to }  } ];
                break;
            }

            default: {
                expressions.DateString = { $regex: new RegExp(`.${period.from}$`) };
            }
        }

        const dates: IDateStats[] = await DateStatsModel.find(expressions);

        res.status(200).json(dates);
        
    } catch (error) {
        errorHandler(res, error);
    }
}

export async function fetchFilterNotes(req: IAccessRequest<TFilterNotesBody>, res: Response<TPaginationRes<IDateNote>>) {

    try {
        const { _id: User } = req.user;
        const { filters, page: { skip, limit } } = req.body;
        
        const { $sort, $match } = completNotesFilters(filters);

        const expressions = [
            {$match: { User, ...$match } },
            {$sort},
            ...paginationAggregate(skip, limit)
        ];
       
        

        const [result] = await DateNoteModel.aggregate([expressions]);
        const rows: IDateNote[] = result ? result.rows : [];
        const totalCount: number = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });
        

    } catch(e) {
        errorHandler(res, e);
    }
}

export async function createDayliNote(req: IAccessRequest<TDNoteCandidate>, res: Response<IDateNote>) {
    try {
        const { _id: User } = req.user;
        const candidate = Object.assign(req.body, { User }) as IDateNote;
        
        const note: IDateNote = await new DateNoteModel(candidate).save();

        const { DateString, DateInt } = note;
        const dateTypes: TDate[] = getDataByArrayKeys(['dateTypes'], DBDayliPlanner);

        const promisesStats = dateTypes.map(Type => {
            const strDate = getDateStringByType(DateString, Type);
            return DateStatsModel.findOne({ User, DateString: strDate, Type });
        });

        const stats: IDateStats[] = await Promise.all(promisesStats);

        const updatePromises = stats.map((stat: IDateStats, idx) => {
            if(stat) {
                stat.NotesCount++;

                return stat.save();
            };

            const Type = dateTypes[idx];
            const instance: CDateStats = new CDateStats(DateInt, Type, User);

            return new DateStatsModel(instance).save();
        })

        await Promise.all(updatePromises);
        
        res.status(200).json(note);

    } catch (error) {
        errorHandler(res, error);
    }
}

export async function getDateNoteById(req: IAccessRequest, res: Response<IDateNote>) {
    try {

        const note: IDateNote = await DateNoteModel.findById(req.params.id);

        res.status(200).json(note)
        
    } catch (error) {
        errorHandler(res, error); 
    }
}

export async function editDateNoteById(req: IAccessRequest<IDateNote>, res: Response<IDateNote>) {
    try {

        const candidate: IDateNote = req.body;
        const { id: noteId } = req.params; 

        const updateNote: IDateNote = await DateNoteModel.findByIdAndUpdate(noteId, 
            { $set: candidate },
            { new: true }
        );
        
        res.status(200).json(updateNote);
        
    } catch (error) {
        errorHandler(res, error);
    }
}

export async function removeDayliNoteById(req: IAccessRequest, res: Response<IMessage>) {
    try {
        const { id } = req.params;
        const { _id: User } = req.user;
        const note: IDateNote = await DateNoteModel.findById(id);
        const { Title } = note;
        const $dec = { $inc: { NotesCount: -1 } };
        const newDoc = { new: true };

        const dateTypes: TDate[] = getDataByArrayKeys(['dateTypes'], DBDayliPlanner);

        const docs = dateTypes.map((Type: TDate) => {
            const DateString: string = getDateStringByType(note.DateString, Type);

            return DateStatsModel.findOneAndUpdate({Type, DateString, User}, $dec, newDoc)
        })

        const updatedDocs = await Promise.all(docs);

        const promises = updatedDocs
            .filter(d => d?.NotesCount <= 0)
            .map(d => d.remove());

        if(promises.length) await Promise.all(promises);

        await note.remove();

        res.status(200).json({ message: `Заметка ${Title} удаленa.` });
        
    } catch (error) {
        errorHandler(res, error);
    }
}