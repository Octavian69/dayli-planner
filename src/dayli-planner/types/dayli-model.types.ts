import { IDateNote } from "../interfaces/IDateNote";
import { IPage } from "../../shared/interfaces/app/IPage";
import { TFromTo } from "../../shared/types/shared.types";
import { IDateStats } from "../interfaces/IDateStats";

export type TDate = 'year' | 'month' | 'date';
export type TDateParameter = string | number | Date;
export type TSplitDate = [string, string, string]; //date, month, year
export type TDNoteCandidate = Pick<IDateNote, 'Title' | 'Description' | 'Priority' | 'DateString'> ;

export type NotesFetchBody<T extends object = {}> = {
    DateString: string;
    page: IPage;
    filters: T
}

export type DateStatsFetchBody = {
    Type: TDate; 
    period: TFromTo<string | number>
}

export type TNoteFilter = {
    SortField: 'Priority' | 'DateInt';
    Direction: 1 | -1;
    Title: string;
    Description: string;
    Priority: number;
    Status: number;
}

export type TFilterNotesBody = {
    filters: TNoteFilter,
    page: IPage
}

