import * as express from 'express';
import * as cors from 'cors';
import * as morgan from 'morgan';
import { initAppRouters } from './shared/handlers/app-router.handlers';
import { authenticate } from './shared/handlers/authenticate.handler';

const app = express();

app.use(morgan('dev') as any); 
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({origin: '*', allowedHeaders: '*'}));
app.use(authenticate)
initAppRouters();

export { app };




