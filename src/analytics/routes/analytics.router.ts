import { Router } from "express";
import { fetchAnalyticsForPeriod } from "../controllers/analytics.controller";
const router: Router = Router();

router.get('/fetch-by-period', fetchAnalyticsForPeriod);

export const analyticsRouter: Router = router;