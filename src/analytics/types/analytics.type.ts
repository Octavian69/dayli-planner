import { IDateStats } from "../../dayli-planner/interfaces/IDateStats";
import { TDate } from "../../dayli-planner/types/dayli-model.types";

export type TAnaliticsPeriodQuery = {
    type: Exclude<TDate, 'date'>;
    period: number;
}
export type AnalyticsResponse = {
    yearStats: IDateStats[],
    monthStats: IDateStats[]
}
