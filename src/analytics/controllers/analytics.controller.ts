import { Response } from "express";
import { errorHandler } from "../../shared/handlers/shared-error.handlers";
import { IAccessRequest } from "../../shared/interfaces/app/IAccessRequest";
import { Simple } from "../../shared/types/shared.types";
import { IDateStats } from "../../dayli-planner/interfaces/IDateStats";
import { DateStatsModel } from "../../dayli-planner/models/MDateStats";


export async function fetchAnalyticsForPeriod(req: IAccessRequest, res: Response<IDateStats[]>) {
    try {

        
        
        const { _id: User } = req.user;
        const { type, period } = req.query;
        console.log('request:', period);

        let expressions: Simple<any>;

        switch(type) {
            case 'year': {
                const now: Date = new Date();
                const currentYear: number = now.getFullYear();
                const fromYear: number = currentYear - Number(period);

                expressions = {
                    Value: { $gte: fromYear, $lte: currentYear },
                    User,
                    Type: 'year',
                }

                break;
            }

            case 'month': {
                const fromMonth: Date = new Date();
                const toMonth: Date = new Date();
        
                // Период за месяц
                fromMonth.setMonth(fromMonth.getMonth() - 1);
                fromMonth.setHours(0, 0, 0, 0);
                toMonth.setHours(23, 59, 59);
                
                 expressions = {
                    DateInt: { $gte: fromMonth.getTime(), $lte: toMonth.getTime() },
                    Type: 'date',
                    User
                }

                break;
            }
        }

        const stats: IDateStats[] = await DateStatsModel.find(expressions).sort('DateInt');
        // stats.sort((a, b) => a.DateInt - b.DateInt);
        res.status(200).json(stats);
        
    } catch (e) {
        errorHandler(res, e);
    }
}