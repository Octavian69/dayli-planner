import * as mongoose from 'mongoose';
import { IToken } from '../interfaces/IToken';

const { Schema, model, Types: {  ObjectId } } = mongoose;

const TokenSchema: mongoose.Schema = new Schema({
    access_token: {
        type: String,
        default: null
    },
    refresh_token: {
        type: String,
        default: null
    },
    User: {
        type: ObjectId,
        refs: 'user',
        required: true,
        unique: true
    }
});

export const TokenModel = model<IToken>('token', TokenSchema);