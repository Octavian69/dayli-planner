export type ValidationStatus = {
    isValid: boolean;
}