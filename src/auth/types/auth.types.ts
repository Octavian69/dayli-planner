import { IUser } from "../../shared/interfaces/models/IUser";
import { IToken } from "../interfaces/IToken";

export type TRefreshTokenBody = Pick<IToken, 'refresh_token'> & Pick<IUser, '_id'>;
export type TUserToken = Pick<IUser, 'Name' | 'Created' | '_id'>;
export type TGenerateTokens = Pick<IToken, 'access_token' | 'refresh_token'>