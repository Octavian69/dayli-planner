import { Router } from 'express';
import { registrationValidators, loginValidators } from '../validators/auth.validators';
import { registrationHandler, loginHandler, refreshTokenHandler, getUserByLogin } from '../controllers/auth.controller';
const router = Router();

router.post('/registration', registrationValidators, registrationHandler);
router.post('/login', loginValidators, loginHandler);
router.post('/refresh', refreshTokenHandler);
router.post('/get-by-login', getUserByLogin);


export const authRouter = router;
