export namespace AuthLength {

    //Name
    export const NAME_MIN: number = 3;
    export const NAME_MAX: number = 25;

    //Password
    export const PASSWORD_MIN: number = 7;
    export const PASSWORD_MAX: number = 20;
}