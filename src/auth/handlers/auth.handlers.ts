import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { TGenerateTokens } from '../types/auth.types';

export function generateTokensHandler<T extends object>(data: T, accessTime: string = '1h', refreshTime: string = '7d'): TGenerateTokens {
    const JWT_KEY: string = config.get('JWT_KEY');
    const access_token = jwt.sign(data, JWT_KEY, { expiresIn: accessTime});
    const refresh_token = jwt.sign(data, JWT_KEY, { expiresIn: refreshTime});

    return { access_token, refresh_token };
}