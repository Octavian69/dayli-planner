import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { Request, Response } from 'express';
import { errorHandler, checkedRequestErrors } from "../../shared/handlers/shared-error.handlers";
import { AUTH_CONST } from '../namespaces/auth.namespaces';
import { IUser } from '../../shared/interfaces/models/IUser';
import { registrationErrorMessages, LoginErrorMessage, accessDeniedMessage } from '../db/auth-messages.db';
import { UserModel } from '../../shared/models/User';
import { TokenModel } from '../models/MToken';
import { IToken } from '../interfaces/IToken';
import { TGenerateTokens, TUserToken, TRefreshTokenBody } from '../types/auth.types';
import { generateTokensHandler } from '../handlers/auth.handlers';
import { IMessage } from '../../shared/interfaces/app/IMessage';
import { IRequest } from '../../shared/interfaces/app/IRequest';


export async function registrationHandler(req: IRequest<IUser>, res: Response<IMessage>) {

    try {
        const { isValid } = checkedRequestErrors(req, res, registrationErrorMessages);
        if(!isValid) return

        const { body } = req;
        const Password: string = await bcrypt.hash(body.Password, AUTH_CONST.SALT);
        const candidate: IUser = Object.assign(body, { Password }); 
        const user = await new UserModel(candidate).save();

        await new TokenModel({ User: user._id }).save();

        res.status(201).json(
            { message: 'Вы создали свой личный кабинет.Используйте данные для входа.' }
        );

    } catch(e) {
        errorHandler(res, e);
    }
}

export async function loginHandler(req: IRequest<Pick<IUser, 'Login' | 'Password'>>, res: Response<TGenerateTokens | IMessage>) {

    try {
        const { isValid } = checkedRequestErrors(req, res, registrationErrorMessages);

        if(!isValid) return;

        const { Login, Password: UserPassword } = req.body;
        
        const user: IUser = await UserModel.findOne({ Login });

        if(!user) {
            return res.status(404).json({ message: LoginErrorMessage })
        }

        const isEqualPassword: boolean = bcrypt.compareSync(UserPassword, user.Password);


        if(isEqualPassword) {
            const { _id, Name, Created } = user;
            const genTokensObj: TGenerateTokens = generateTokensHandler({_id, Name, Created});

            await TokenModel.findOneAndUpdate(
                { User: _id }, 
                { $set: genTokensObj }
            )

            return res.status(200).json(genTokensObj);
        }

        res.status(406).json({ message: LoginErrorMessage });

    } catch(e) {
        errorHandler(res, e);
    }
}

export async function refreshTokenHandler(req: IRequest<TRefreshTokenBody>, res: Response<TGenerateTokens | IMessage>) {

    try {
        const JWT_KEY = config.get('JWT_KEY');
        const { refresh_token, _id: User } = req.body;

        const token: IToken = await TokenModel.findOne({ User });
        
        jwt.verify(refresh_token, JWT_KEY, async (err: jwt.VerifyErrors, decoded) => {
            const isEqual = Object.is(refresh_token, token.refresh_token);

            if(err || !isEqual) return res.status(401).json({ message: accessDeniedMessage});

            const { _id, Name, Created } = decoded as TUserToken;
            const genTokensObj: TGenerateTokens = generateTokensHandler({_id, Name, Created}); 
            token.access_token = genTokensObj.access_token;
            token.refresh_token = genTokensObj.refresh_token;

            await token.save();

            res.status(200).json(genTokensObj);

        });

    } catch(e) {
        errorHandler(res, e, 401)
    }
}

export async function getUserByLogin(req: IRequest<Pick<IUser, 'Login'>>, res: Response) {
    const { Login } = req.body;
    const user: IUser = await UserModel.findOne({ Login });

    res.status(200).json(user);
}