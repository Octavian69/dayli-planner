import { AuthLength } from "../namespaces/auth-validation.namespaces";
import { Simple } from "../../shared/types/shared.types";


export const accessDeniedMessage: string = 'Используйте свои учетные данные для входа в систему.';

export const registrationErrorMessages: Simple<string> = {
    Login: `Поле "Логин" должно быть типа email: ivanov@gmail.com`,
    Name: `Имя должно иметь от ${AuthLength.NAME_MIN} до ${AuthLength.NAME_MAX} символов.`,
    Password: `Пароль должно иметь от ${AuthLength.PASSWORD_MIN} до ${AuthLength.PASSWORD_MAX} символов.`
}

export const LoginErrorMessage: string = 'Ошибка при вводе учетных данных.Попробуйте еще раз';