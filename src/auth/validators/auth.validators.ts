import { check, ValidationChain } from 'express-validator';
import { AuthLength } from '../namespaces/auth-validation.namespaces';

export const registrationValidators: ValidationChain[] = [
    check('Name').exists().isLength({min: AuthLength.NAME_MIN, max: AuthLength.NAME_MAX}),
    check('Login').exists().isEmail().normalizeEmail({ all_lowercase: true }),
    check('Password').exists().isLength({ min: AuthLength.PASSWORD_MIN, max: AuthLength.PASSWORD_MAX })
];

export const loginValidators: ValidationChain[] = [
    check('Login').exists().isEmail().normalizeEmail({ all_lowercase: true }),
    check('Password').exists().isLength({ min: AuthLength.PASSWORD_MIN, max: AuthLength.PASSWORD_MAX })
]


