import { Request } from 'express';

export interface IRequest<T extends object = null> extends Request {
    body: T
}