import { Router } from "express";

export interface IRouteItem {
    url: string;
    router: Router;
}