import { Request } from 'express';
import { TUserToken } from '../../../auth/types/auth.types';

export interface IAccessRequest<T extends object = null> extends Request {
    body: T;
    user?: TUserToken;
}