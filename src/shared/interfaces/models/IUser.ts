import { Document } from 'mongoose';

export interface IUser extends Document {
    Name: string;
    Login: string;
    Password: string;
    Created: Date;
    id?: string;
}