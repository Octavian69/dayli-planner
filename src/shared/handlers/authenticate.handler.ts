import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { Response, NextFunction } from 'express';
import { blackListRoutes } from "../db/app-auth.db";
import { IAccessRequest } from '../interfaces/app/IAccessRequest';
import { accessDeniedMessage } from '../../auth/db/auth-messages.db';
import { TUserToken } from '../../auth/types/auth.types';


export async function authenticate(req: IAccessRequest, res: Response, next: NextFunction) {
    const isOptionsMethod: boolean = req.method === 'OPTIONS';
    const isBlackListRoute: boolean = blackListRoutes.some(route => req.url.includes(route));
    const isNext: boolean = isOptionsMethod || isBlackListRoute;

    if(isNext) return next();
    const JWT_KEY: string = config.get('JWT_KEY');
    const TOKEN_PREFIX: string = config.get('TOKEN_PREFIX');
    
    const token: string = req.headers.authorization.replace(`${TOKEN_PREFIX}`, '');
    
    jwt.verify(token, JWT_KEY, (err: jwt.VerifyErrors, decoded) => {
        if(!err) {
            req.user = decoded as TUserToken;
            return next();
        }

        switch(true) {
            case err instanceof jwt.TokenExpiredError: {
                console.log('TokenExpiredError');
                res.status(426).json(null);
                break;
            }

            default: {
                console.log('TokenAnyError', err);
                res.status(401).json({ message: accessDeniedMessage })
            }
        }
    })
}