import { ValidationError, Result, validationResult } from "express-validator";
import { Request, Response, NextFunction } from 'express';
import { Simple } from "../types/shared.types";
import { ValidationStatus } from "../../auth/types/auth-validation.types";

export function errorHandler(res: Response, error: Error, status: number = 500): void {
    console.log(error)
    res.status(status).json(error);
}

export function completeErrors(errors: ValidationError[], msgs: Simple<string>): ValidationError[] {

    return errors.map(err => {
        const { param } = err;

        if(msgs[param]) err.msg = msgs[param];

        return err;
    })
}

export function checkedRequestErrors(req: Request, res: Response, msgs: Simple<string> = {}): ValidationStatus {
    const errors: Result<ValidationError> = validationResult(req);

    if(!errors.isEmpty()) {
        const completed = completeErrors(errors.array(), msgs)
        res.status(400).json(completed);
        return { isValid: false }
    }

    return { isValid: true }
}
