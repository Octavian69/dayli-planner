export function deepCopy<T>(value: T): T {
  return JSON.parse(JSON.stringify(value));  
}

export function getDataByArrayKeys<T, D extends object>(keys: Array<string>, database: D, isCopy: boolean = true): T {
    let idx: number = 1;
    let value: T = database[keys[0]];

    while(keys[idx]) {
        const key = keys[idx];
        value = value[key];

        if(!value) {
            value = null;
            break;
        }

        idx++;
    }

    return isCopy ? deepCopy(value as T) : value;
}