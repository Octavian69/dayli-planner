import { app } from "../../app";
import { Router } from "express";
import { AppRoutes } from "../db/app-router.db";
import { IRouteItem } from "../interfaces/app/IRouteItem";

function initModuleRouterHandler(url: string, router: Router): void {
    app.use(url, router);
}

export function initAppRouters(): void {
    (AppRoutes as Readonly<IRouteItem[]>).forEach(({ url, router }) => {
        initModuleRouterHandler(url, router);
    });
}