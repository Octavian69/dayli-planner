export function paginationAggregate(skip: number, limit: number): Array<object> {
    return [
        { $group: {
            _id: null,
            totalCount: { $sum: 1 },
            results: { $push: '$$ROOT' }
        } },
        
        { $project: {
            totalCount: 1,
            rows: { $slice: ['$results', skip * limit, +limit] }
        } }
    ]
};

