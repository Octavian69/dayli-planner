export type TFieldFilterType = '$sort' | '$match';

export type Simple<T> = {
    [key: string]: T
}

export type TFromTo<T = number> = {
    from: T,
    to?: T 
}

export type TPaginationRes<T> = {
    rows: T[],
    totalCount: number
}

