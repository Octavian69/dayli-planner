import { IRouteItem } from "../interfaces/app/IRouteItem";
import { authRouter } from "../../auth/routes/auth.router";
import { dayliPlannerRouter } from "../../dayli-planner/routes/dayli-planer.router";
import { analyticsRouter } from "../../analytics/routes/analytics.router";

export const AppRoutes: Readonly<IRouteItem[]> = [
    { url: '/api/auth', router: authRouter },
    { url: '/api/dayli-planner', router: dayliPlannerRouter },
    { url: '/api/analytics', router: analyticsRouter },
]