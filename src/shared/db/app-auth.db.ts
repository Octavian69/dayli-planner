export const blackListRoutes: Readonly<string[]> = [
    'auth/login',
    'auth/registration',
    'auth/refresh',
    'auth/get-by-login'
];