import * as mongoose from 'mongoose';
import { IUser } from '../interfaces/models/IUser';

const { Schema, model } = mongoose;

const UserSchema: mongoose.Schema = new Schema({
    Name: {
        type: String,
        required: true
    },
    Login: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true
    },
    Created: {
        type: Date,
        default: Date.now
    }
});

const UserModel = model<IUser>('user', UserSchema);

export { UserModel }; 