import { app } from "./app";
import * as config from 'config';
import * as mongoose from 'mongoose';
const PORT = process.env.PORT || 5000;
const MONGO_URI = config.get('MONGO_URI');


function start(): void {
    
    if(MONGO_URI) {
        mongoose.connect(MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        }).then(_ => {
    
            console.log('MongoDB connected');
    
            app.listen(PORT, () => {
                console.log(`Server has been started on port: ${PORT}`);
            })
        })
    } else {
        throw new Error('Server process errors');
    }
}

start();



